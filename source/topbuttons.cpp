#include "topbuttons.h"
#include "pages.h"

TopButtons::TopButtons(QString stringOfWindow)
{
    pushButtonHome = new DoubleclickableButton;
    pushButtonStatus = new DoubleclickableButton;
    pushButtonChart = new DoubleclickableButton;
    pushButtonFCConfig = new DoubleclickableButton;
    pushButton2DVision = new DoubleclickableButton;
    pushButton3DView = new DoubleclickableButton;
    pushButtonRC = new DoubleclickableButton;
    pushButtonSerial = new DoubleclickableButton;
    pushButtonNetwork = new DoubleclickableButton;
    pushButtonSettings = new DoubleclickableButton;
    spacerLeft = new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Fixed);
    spacerRight = new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Fixed);
    QString stringStyleSHeet = "QPushButton{border-radius: 4px;padding:8px 0px;border: 2px solid #2D8DFF;"
        "background-color: white;}"
        "QPushButton:hover {background-color: #7ED1FF;color: white;}"
        "QPushButton:pressed {background-color: #2D8DFF;color: white;}"
        "QPushButton:disabled {background-color: rgba(45,141,255,100%);color: white;}";

    Pages::setPagesStringList(&stringList);

    pushButtonHome->setText(stringList.at(Pages::HomePage));
    pushButtonStatus->setText(stringList.at(Pages::StatusPage));
    pushButtonChart->setText(stringList.at(Pages::ChartPage));
    pushButtonFCConfig->setText(stringList.at(Pages::FCConfigPage));
    pushButton2DVision->setText(stringList.at(Pages::_2DVisionPage));
    pushButton3DView->setText(stringList.at(Pages::_3DViewPage));
    pushButtonRC->setText(stringList.at(Pages::RCPage));
    pushButtonSerial->setText(stringList.at(Pages::SerialPage));
    pushButtonNetwork->setText(stringList.at(Pages::Networkpage));
    pushButtonSettings->setText(stringList.at(Pages::SettingsPage));

    pushButtonHome->setStyleSheet(stringStyleSHeet);
    pushButtonStatus->setStyleSheet(stringStyleSHeet);
    pushButtonChart->setStyleSheet(stringStyleSHeet);
    pushButtonFCConfig->setStyleSheet(stringStyleSHeet);
    pushButton2DVision->setStyleSheet(stringStyleSHeet);
    pushButton3DView->setStyleSheet(stringStyleSHeet);
    pushButtonRC->setStyleSheet(stringStyleSHeet);
    pushButtonSerial->setStyleSheet(stringStyleSHeet);
    pushButtonNetwork->setStyleSheet(stringStyleSHeet);
    pushButtonSettings->setStyleSheet(stringStyleSHeet);

    this->addSpacerItem(spacerLeft);
    this->addWidget(pushButtonHome);
    this->addWidget(pushButtonStatus);
    this->addWidget(pushButtonChart);
    this->addWidget(pushButtonFCConfig);
    this->addWidget(pushButton2DVision);
    this->addWidget(pushButton3DView);
    this->addWidget(pushButtonRC);
    this->addWidget(pushButtonSerial);
    this->addWidget(pushButtonNetwork);
    this->addWidget(pushButtonSettings);
    this->addSpacerItem(spacerRight);

    int buttonMinimumWidth = pushButtonStatus->fontInfo().pixelSize() * 5;\
    LOGI("buttonMinimumWidth:%d", buttonMinimumWidth);
    pushButtonHome->setMinimumWidth(buttonMinimumWidth);
    pushButtonStatus->setMinimumWidth(buttonMinimumWidth);
    pushButtonChart->setMinimumWidth(buttonMinimumWidth);
    pushButtonFCConfig->setMinimumWidth(buttonMinimumWidth);
    pushButton2DVision->setMinimumWidth(buttonMinimumWidth);
    pushButton3DView->setMinimumWidth(buttonMinimumWidth);
    pushButtonRC->setMinimumWidth(buttonMinimumWidth);
    pushButtonSerial->setMinimumWidth(buttonMinimumWidth);
    pushButtonNetwork->setMinimumWidth(buttonMinimumWidth);
    pushButtonSettings->setMinimumWidth(buttonMinimumWidth);

    switch (stringList.indexOf(stringOfWindow)) {
    case Pages::HomePage:
        pushButtonHome->setDisabled(1);
        break;
    case Pages::StatusPage:
        pushButtonStatus->setDisabled(1);
        break;
    case Pages::ChartPage:
        pushButtonChart->setDisabled(1);
        break;
    case Pages::FCConfigPage:
        pushButtonFCConfig->setDisabled(1);
        break;
    case Pages::_2DVisionPage:
        pushButton2DVision->setDisabled(1);
        break;
    case Pages::_3DViewPage:
        pushButton3DView->setDisabled(1);
        break;
    case Pages::RCPage:
        pushButtonRC->setDisabled(1);
        break;
    case Pages::SerialPage:
        pushButtonSerial->setDisabled(1);
        break;
    case Pages::Networkpage:
        pushButtonNetwork->setDisabled(1);
        break;
    case Pages::SettingsPage:
        pushButtonSettings->setDisabled(1);
        break;
    default :
        pushButtonHome->setDisabled(1);
        break;
    }

    this->setMargin(0);
    this->setSpacing(0);
}

int TopButtons::connectButtonsToMainWindow(MainWindow *mainWindow)
{
    connect(pushButtonHome,&DoubleclickableButton::singleClicked,
            mainWindow,&MainWindow::mainWindowSetLayout);
    connect(pushButtonHome,&DoubleclickableButton::doubleClicked,
            mainWindow,&MainWindow::newWindow);
    connect(pushButtonStatus,&DoubleclickableButton::singleClicked,
            mainWindow,&MainWindow::mainWindowSetLayout);
    connect(pushButtonStatus,&DoubleclickableButton::doubleClicked,
            mainWindow,&MainWindow::newWindow);
    // TODO: pushButtonChart
    // TODO: pushButtonFCConfig
    // TODO: pushButton2DVision
    // TODO: pushButton3DView
    connect(pushButtonRC,&DoubleclickableButton::singleClicked,
            mainWindow,&MainWindow::mainWindowSetLayout);
    connect(pushButtonRC,&DoubleclickableButton::doubleClicked,
            mainWindow,&MainWindow::newWindow);
    // TODO: pushButtonSerial
    connect(pushButtonNetwork,&DoubleclickableButton::singleClicked,
            mainWindow,&MainWindow::mainWindowSetLayout);
    connect(pushButtonNetwork,&DoubleclickableButton::doubleClicked,
            mainWindow,&MainWindow::newWindow);
    connect(pushButtonSettings,&DoubleclickableButton::singleClicked,
            mainWindow,&MainWindow::mainWindowSetLayout);
    connect(pushButtonSettings,&DoubleclickableButton::doubleClicked,
            mainWindow,&MainWindow::newWindow);
    return 0;
}

int TopButtons::disconnectButtonsToMainWindow()
{
    // DoubleclickableButton不能全部断开，否则无法触发
    disconnect(pushButtonHome,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonHome,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButtonStatus,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonStatus,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButtonChart,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonChart,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButtonFCConfig,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonFCConfig,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButton2DVision,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButton2DVision,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButton3DView,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButton3DView,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButtonRC,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonRC,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButtonSerial,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonSerial,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButtonNetwork,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonNetwork,&DoubleclickableButton::doubleClicked,0,0);
    disconnect(pushButtonSettings,&DoubleclickableButton::singleClicked,0,0);
    disconnect(pushButtonSettings,&DoubleclickableButton::doubleClicked,0,0);
    return 0;
}
