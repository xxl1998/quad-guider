#include "doubleclickablebutton.h"
#include <QDebug>

DoubleclickableButton::DoubleclickableButton(QWidget *parent) : QPushButton(parent)
{
    m_clicked = false;
    connect(this, &QPushButton::clicked, this, &DoubleclickableButton::slotClicked);
    m_timer = nullptr;
}

void DoubleclickableButton::slotClicked()
{
//    qDebug() << "slotClicked";
    if(m_clicked == false){
        m_timer = new QTimer;
        connect(m_timer, &QTimer::timeout, this, &DoubleclickableButton::slotTimerTimeOut);
        m_clicked = true;
        m_timer->start(300);
    }else {
        if(m_timer != nullptr){
            m_timer->stop();
            delete m_timer;
            m_timer = nullptr;
        }
        m_clicked = false;
//        qDebug() << "doubleClicked";
        emit doubleClicked();
    }
}

void DoubleclickableButton::slotTimerTimeOut()
{
    m_clicked = false;
//    qDebug() << "singleClicked";
    emit singleClicked();
    delete m_timer;
    m_timer = nullptr;
}
