#include "homewidget.h"
#include <common/mavlink.h>

QMutex HomeWidget::mutex;
HomeWidget* HomeWidget::instance = NULL;

HomeWidget::HomeWidget(QWidget *parent) : QWidget(parent)
{
    LOGD("here");
    // 创建对象
    labelVideo = new QLabel;// 显示回传图像
    imageVideo = new QImage;// 缓存回传图像
    vBoxLayoutPage = new QVBoxLayout;// 当前页面最外层的纵向布局
    vBoxLayoutLabel = new QVBoxLayout;// labelVideo的布局，包含顶部按钮、中间信息、下方曲线
    topButtons = new TopButtons(QString::fromStdString(Pages::pagesString[Pages::HomePage]));// 顶部按钮
    spacerUnderTopButtons = new QSpacerItem(0,0,QSizePolicy::Fixed,QSizePolicy::Expanding);
    spacerOnLabels = new QSpacerItem(0,0,QSizePolicy::Fixed,QSizePolicy::Expanding);
    hBoxLayoutLabels = new QHBoxLayout;// 包含中间的所有label
    vBoxLayoutLabelsLeft = new QVBoxLayout;// 包含左边的label
    labelsLeft0 = new QLabel("Pitch:0.00");
    labelsLeft1 = new QLabel("Roll:0.00");
    labelsLeft2 = new QLabel("Yaw:0.00");
    // 两边labels中间的spacer
    spacerLabelsCenter = new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Fixed);
    vBoxLayoutLabelsRight = new QVBoxLayout;// 包含右边的label
    labelsRight0 = new QLabel("PosX:0.00");
    labelsRight1 = new QLabel("PosY:0.00");
    labelsRight2 = new QLabel("PosZ:0.00");
    spacerUnderLabels = new QSpacerItem(0,0,QSizePolicy::Fixed,QSizePolicy::Expanding);

    chartView = new QtCharts::QChartView;
    lineSeries0 = new QtCharts::QLineSeries();// 曲线的数据
    lineSeries1 = new QtCharts::QLineSeries();
    chart = new QtCharts::QChart();

    // 加载图片，在没有回传图像时显示静态图片
    imageVideo->load("../resource/drone.png");// TODO：使用设置里面保存的路径
    labelVideo->setPixmap(QPixmap::fromImage(*imageVideo));
    // 调整label的大小与图片大小一致，会影响整个窗口的大小
    labelVideo->resize(imageVideo->width(), imageVideo->height());

    // labelVideo上面叠加的内容
    vBoxLayoutLabel->setMargin(0);
    vBoxLayoutLabel->setSpacing(0);
    // 设置背景半透明
    QString stringStyleSheetLabels = "background-color: rgba(255, 255, 255, 50%);";
    labelsLeft0->setStyleSheet(stringStyleSheetLabels);
    labelsLeft1->setStyleSheet(stringStyleSheetLabels);
    labelsLeft2->setStyleSheet(stringStyleSheetLabels);
    labelsRight0->setStyleSheet(stringStyleSheetLabels);
    labelsRight1->setStyleSheet(stringStyleSheetLabels);
    labelsRight2->setStyleSheet(stringStyleSheetLabels);


    // 左边的label
    vBoxLayoutLabelsLeft->setMargin(0);
    vBoxLayoutLabelsLeft->addWidget(labelsLeft0);
    vBoxLayoutLabelsLeft->addWidget(labelsLeft1);
    vBoxLayoutLabelsLeft->addWidget(labelsLeft2);
    // 右边的label
    vBoxLayoutLabelsRight->setMargin(0);
    vBoxLayoutLabelsRight->addWidget(labelsRight0);
    vBoxLayoutLabelsRight->addWidget(labelsRight1);
    vBoxLayoutLabelsRight->addWidget(labelsRight2);
    // 两边的labels
    hBoxLayoutLabels->setMargin(0);
    hBoxLayoutLabels->addLayout(vBoxLayoutLabelsLeft);
    hBoxLayoutLabels->addSpacerItem(spacerLabelsCenter);
    hBoxLayoutLabels->addLayout(vBoxLayoutLabelsRight);
    // 添加spacer
    vBoxLayoutLabel->addSpacerItem(spacerOnLabels);
    // 添加labels
    vBoxLayoutLabel->addLayout(hBoxLayoutLabels);
    // 添加spacer
    vBoxLayoutLabel->addSpacerItem(spacerUnderLabels);

    // 添加数据
    lineSeries0->setName("data0");// TODO:可配置
    lineSeries0->append(0, 6);
    lineSeries0->append(2, 4);
    lineSeries0->append(3, 8);
    lineSeries0->append(7, 4);
    lineSeries0->append(10, 5);
    *lineSeries0 << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);
    lineSeries1->setName("data1");
    *lineSeries1 << QPointF(11, 5) << QPointF(13, 4) << QPointF(17, 7) << QPointF(18, 2) << QPointF(20, 1);
    // 添加到曲线
    chart->addSeries(lineSeries0);
    chart->addSeries(lineSeries1);
    chart->createDefaultAxes();
//    chart->setTitle("Chart of AutoPilot data");
    // 曲线的图例
    chart->legend()->setVisible(true);
    chart->legend()->detachFromChart();
    chart->legend()->setBackgroundVisible(false);
    int legendFontSize = chart->legend()->font().pointSize();
    LOGD("legendFontSize:%d",legendFontSize);
    chart->legend()->layout()->setContentsMargins(0, 0, 0, 0);
    chart->legend()->setGeometry(QRectF(legendFontSize * 6, 0, 150, legendFontSize*10));
    chart->legend()->setAlignment(Qt::AlignTop);
    // 背景透明，去除间距
    chart->setBackgroundVisible(false);
    chart->setContentsMargins(0, 0, 0, 0);
    chart->setMargins(QMargins(0, 0, 0, 0));
    chart->layout()->setContentsMargins(0, 0, 0, 0);
    // 显示曲线
    chartView->setChart(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setStyleSheet("background: transparent;border:0px;padding:0px 0px;");
    chartView->setMaximumHeight(labelVideo->height() * 0.3);
    vBoxLayoutLabel->addWidget(chartView);

    // 设置图片叠加布局
    labelVideo->setLayout(vBoxLayoutLabel);

    // 主界面最外层布局
    vBoxLayoutPage->setMargin(0);// 每一个layout都要设置间距为0
    vBoxLayoutPage->setSpacing(0);
    // 添加顶部按钮
    vBoxLayoutPage->addLayout(topButtons);
    vBoxLayoutPage->addSpacerItem(spacerUnderTopButtons);
    // 添加labelVideo
    vBoxLayoutPage->addWidget(labelVideo);
    this->setLayout(vBoxLayoutPage);
}

HomeWidget::~HomeWidget()
{
    LOGD("here");
//    delete labelVideo;
}

void HomeWidget::updateAttitude(void *p)
{
    mavlink_attitude_t *attitude = (mavlink_attitude_t *)p;
    QString stringPitch = "Pitch:";
    QString stringRoll = "Roll:";
    QString stringYaw = "Yaw:";
    float rad2deg = 180.0 / 3.1416;
    stringPitch += QString::number(attitude->pitch * rad2deg, 'f', 2);
    stringRoll += QString::number(attitude->roll * rad2deg, 'f', 2);
    stringYaw += QString::number(attitude->yaw * rad2deg, 'f', 2);
    labelsLeft0->setText(stringPitch);
    labelsLeft1->setText(stringRoll);
    labelsLeft2->setText(stringYaw);
}
