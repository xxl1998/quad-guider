#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LOGV("start application now");
    MainWindow *w = new MainWindow;
    w->show();
    return a.exec();
}
