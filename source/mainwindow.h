#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QStatusBar>
#include "pages.h"
#include "log.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, Pages::PagesNumber pageType = Pages::HomePage);
    ~MainWindow();
    int mainWindowSetLayout();
    int newWindow();
    int removeCurrentWidget();
    int disconnectCurrentWidget();
    int setPageAndConnectButtons(Pages::PagesNumber pageType);

    // 多窗口相关
    static MainWindow *getWindowByPage(QWidget *pageInstance);
    static MainWindow *getWindowByPage(Pages::PagesNumber pageType);
    static unsigned int getNumberOfAllWindows(void);

    Pages::PagesNumber currentPageNumber;
    QWidget *mainWindowCentralWidget;
    QHBoxLayout *hBoxLayoutCentralWidget;
    QStatusBar *windowStatusBar;
    QLabel *labelStatusHostPlatform;
    QLabel *labelStatusArmed;
    QLabel *labelStatusFlyMode;
    QLabel *labelStatusUDPSpeed;
    QLabel *labelStatusSerialSpeed;
    QLabel *labelStatusFCConnected;
    QLabel *labelStatusHeartbeat;

private:

};
#endif // MAINWINDOW_H
