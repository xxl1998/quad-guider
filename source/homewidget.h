#ifndef HOMEWIDGET_H
#define HOMEWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QtWidgets>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QSpacerItem>
#include <QGraphicsOpacityEffect>
#include <QLineSeries>
#include <QFontDatabase>
#include <QChart>
#include "qchartview.h"
#include "mainwindow.h"
#include "topbuttons.h"
#include "pages.h"

class HomeWidget : public QWidget
{
    Q_OBJECT
public:
    ~HomeWidget();
    static HomeWidget& getInstance(){
        if(NULL == instance){
            mutex.lock();
            if(NULL == instance){
                instance = new HomeWidget();
            }
            mutex.unlock();
        }
        return *instance;
    }

    static void release(){
        if(instance != NULL){
            mutex.lock();
            delete instance;
            instance = NULL;
            mutex.unlock();
        }
    }


    QLabel *labelVideo;
    QImage *imageVideo;

    QVBoxLayout *vBoxLayoutPage;
    QVBoxLayout *vBoxLayoutLabel;
    TopButtons *topButtons;
    QSpacerItem *spacerUnderTopButtons;
    QSpacerItem *spacerOnLabels;
    QHBoxLayout *hBoxLayoutLabels;
    QVBoxLayout *vBoxLayoutLabelsLeft;
    QLabel *labelsLeft0;
    QLabel *labelsLeft1;
    QLabel *labelsLeft2;
    QSpacerItem *spacerLabelsCenter;
    QVBoxLayout *vBoxLayoutLabelsRight;
    QLabel *labelsRight0;
    QLabel *labelsRight1;
    QLabel *labelsRight2;
    QSpacerItem *spacerUnderLabels;

    QtCharts::QChartView *chartView;
    QtCharts::QChart *chart;
    QtCharts::QLineSeries *lineSeries0;
    QtCharts::QLineSeries *lineSeries1;

public slots:
    void updateAttitude(void *p);

private:
    explicit HomeWidget(QWidget *parent = nullptr);
    HomeWidget(const HomeWidget&other); //拷贝构造函数
    HomeWidget& operator=(const HomeWidget&other); //赋值运算操作符

    static QMutex mutex;
    static HomeWidget* instance;

signals:

};

#endif // HOMEWIDGET_H
