QT       += core gui network
QT       += charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DESTDIR = "$$PWD/../executable"

DEFINES += USE_ASSIMP_FOR_3D_MODEL_VIEWER # 不使用assimp时注释这一行

SOURCES += \
    doubleclickablebutton.cpp \
    homewidget.cpp \
    log.cpp \
    main.cpp \
    mainwindow.cpp \
    mavlinkdataprocessor.cpp \
    networkwidget.cpp \
    rcwidget.cpp \
    settingsmanager.cpp \
    settingswidget.cpp \
    statuswidget.cpp \
    topbuttons.cpp

HEADERS += \
    doubleclickablebutton.h \
    homewidget.h \
    log.h \
    mainwindow.h \
    mavlinkdataprocessor.h \
    networkwidget.h \
    pages.h \
    rcwidget.h \
    settingsmanager.h \
    settingswidget.h \
    statuswidget.h \
    topbuttons.h

# mavlink
INCLUDEPATH+=$$PWD/../3rd_party/mavlink_c_library_v2
INCLUDEPATH+=$$PWD/../3rd_party/mavlink_c_library_v2/common

if(contains(DEFINES,USE_ASSIMP_FOR_3D_MODEL_VIEWER)){
SOURCES += \
    modelloader.cpp \
    modelviewwidget.cpp

HEADERS += \
    modelloader.h \
    modelviewwidget.h
}


QMAKE_SPEC_T = $$[QMAKE_SPEC]
message(QMAKE_SPEC:$${QMAKE_SPEC_T})
message(QT_ARCH:$${QT_ARCH})
message(QMAKE_HOST.arch:$$QMAKE_HOST.arch)
message(QMAKE_HOST.os:$$QMAKE_HOST.os)
message(QMAKE_HOST.cpu_count:$$QMAKE_HOST.cpu_count)
message(QMAKE_HOST.name:$$QMAKE_HOST.name)
message(QMAKE_HOST.version:$$QMAKE_HOST.version)
message(QMAKE_HOST.version_string:$$QMAKE_HOST.version_string)

isEqual(QMAKE_HOST.os, Windows):{
    DEFINES += __HOST_PLATFORM_WINDOWS_
    message("define __HOST_PLATFORM_WINDOWS_")
    INCLUDEPATH+=C:\Qt\opencv_install\include
    LIBS+=C:\Qt\opencv_build\lib\libopencv_*.a
    LIBS += -L$$PWD/../3rd_party/assimp_win_lib/bin  -lassimp-5
    INCLUDEPATH+=$$PWD/../3rd_party/assimp_win_lib/include

    version_update.commands = $$PWD/../scripts/update_version.bat
    version_update.depends = FORCE
    QMAKE_EXTRA_TARGETS += version_update
    PRE_TARGETDEPS += version_update
}

isEqual(QMAKE_HOST.os, Linux):{
    DEFINES += __HOST_PLATFORM_LINUX_
    message("define __HOST_PLATFORM_LINUX_")
    INCLUDEPATH += /usr/local/include \
                    /usr/local/include/opencv \
                    /usr/local/include/opencv4
    LIBS += /usr/local/lib/libopencv_*
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
