#include "settingsmanager.h"
#include <QFile>
#include <QMessageBox>
#include "log.h"

QMutex SettingsManager::mutex;
SettingsManager* SettingsManager::instance = NULL;

SettingsManager::SettingsManager()
{
    useDefaultConfigFile = true;
    pathPrefix = "config/";
    QString defaultConfigFilePath = pathPrefix + defaultConfigFile;
    if(!QFile::exists(defaultConfigFilePath)){
        pathPrefix.insert(0, "../resource/");
        defaultConfigFilePath = pathPrefix + defaultConfigFile;
        if(!QFile::exists(defaultConfigFilePath)){
            LOGE("no config files found!!!!!");
            qDebug() << "no config files found!!!!!";
            // TODO: auto create config file
            return;
        }
    }
    LOGI("config file found:%s", defaultConfigFilePath.toStdString().data());

    configIniFile = new QSettings(defaultConfigFilePath, QSettings::IniFormat);
    configFileName = configIniFile->value("/APP/CONFIG_FILE").toString();
    QString configFilePath = pathPrefix + configFileName;
    if(QFile::exists(configFilePath)){
        LOGI("use config file:%s", configFilePath.toStdString().data());
        delete configIniFile;
        configIniFile = new QSettings(configFilePath, QSettings::IniFormat);
        useDefaultConfigFile = false;
    }else {
        LOGI("use default config file:%s", defaultConfigFilePath.toStdString().data());
        useDefaultConfigFile = true;
        configFileName = defaultConfigFile;
    }
    LOGI("config file init OK now");
}

SettingsManager::~SettingsManager()
{
    LOGD("here");
    delete configIniFile;
}

void SettingsManager::setValue(const QString key, const QString value){
    configIniFile->setValue(key, value);
}

QString SettingsManager::getValue(const QString key){
    return configIniFile->value(key).toString();
}

int SettingsManager::setConfigFile(const QString path)
{
    if(!QFile::exists(pathPrefix + path)){
        LOGE("config file not found:%s", QString(pathPrefix + path).toStdString().data());
        qDebug() << "config file not found:" << pathPrefix + path;
        QMessageBox::warning(nullptr,
            "Config file not found",
            "Config file: " + pathPrefix + path + " not found, please check!!!",
            QMessageBox::Ok,
            QMessageBox::Ok);
        return -1;
    }
    if(!QFile::exists(pathPrefix + defaultConfigFile)){
        LOGE("config file not found:%s", QString(pathPrefix + defaultConfigFile).toStdString().data());
        qDebug() << "default config file not found:" << pathPrefix + defaultConfigFile;
        QMessageBox::warning(nullptr,
            "Default config file not found",
            "Default config file: " + pathPrefix + defaultConfigFile + " not found, please check!!!",
            QMessageBox::Ok,
            QMessageBox::Ok);
        return -1;
    }
    LOGI("config file found:%s", QString(pathPrefix + path).toStdString().data());

    if(useDefaultConfigFile){
        configIniFile->setValue(keyConfigFile, path);
    }else{
        QSettings *configIniFileDefault = new QSettings(pathPrefix + defaultConfigFile, QSettings::IniFormat);
        configIniFileDefault->setValue(keyConfigFile, path);
        delete configIniFileDefault;
    }
    return 0;
}
