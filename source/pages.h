#ifndef PAGES_H
#define PAGES_H
#include <QStringList>

namespace Pages
{
    enum PagesNumber {
        HomePage,
        StatusPage,
        ChartPage,
        FCConfigPage,
        _2DVisionPage,
        _3DViewPage,
        RCPage,
        SerialPage,
        Networkpage,
        SettingsPage,
    };

    const std::string pagesString[10] = {"Home", "Status", "Chart", "FC Config", "2D Vision",
                                 "3D View", "RC", "Serial", "Network", "Settings"};

    inline void setPagesStringList(QStringList *stringList){
        *stringList << "Home" << "Status" << "Chart" << "FC Config" << "2D Vision"
                    << "3D View" << "RC" << "Serial" << "Network" << "Settings";
    }
}

#endif // PAGES_H
