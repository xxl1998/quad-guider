#include "rcwidget.h"

QMutex RcWidget::mutex;
RcWidget* RcWidget::instance = NULL;

RcWidget::RcWidget(QWidget *parent) : QWidget(parent)
{
    vBoxLayoutPage = new QVBoxLayout;// 当前页面最外层的纵向布局
    topButtons = new TopButtons(QString::fromStdString(Pages::pagesString[Pages::RCPage]));
    spacerUnderTopButtons = new QSpacerItem(0,0,QSizePolicy::Fixed,QSizePolicy::Expanding);

    // 当前界面最外层布局
    vBoxLayoutPage->setMargin(0);// 每一个layout都要设置间距为0
    vBoxLayoutPage->setSpacing(0);

    // 添加顶部按钮
    vBoxLayoutPage->addLayout(topButtons);
    vBoxLayoutPage->addSpacerItem(spacerUnderTopButtons);

    this->setLayout(vBoxLayoutPage);
}

RcWidget::~RcWidget()
{
    LOGD("here");
}
