#ifndef MAVLINKDATAPROCESSOR_H
#define MAVLINKDATAPROCESSOR_H
#include <QWidget>
#include <QtWidgets>
#include <QDebug>
#include <QObject>
#include <QThread>
#include <common/mavlink.h>

extern QMutex mutexListUdpReceivedData;
extern QList<QByteArray *> listUdpReceivedData;

class MavlinkDataProcessor:public QObject
{
    Q_OBJECT
public:
    MavlinkDataProcessor(QObject* parent = nullptr)
    {
        stop = false;
    }
    bool stop;

public slots:
     // this function run in thread
    void processData();

signals:
    // UDP原始数据字符串，用于更新UI
    void stringReady(QString string);
    // 姿态数据
    void attitudeReady(void *p);
    void messageReady(mavlink_message_t msg);
};

template<typename T> class MavlinkPayloadManager
{
public:
    MavlinkPayloadManager()
    {
        //
    }
    QList<T *> list;
    QReadWriteLock lock;
    T* read()
    {
        lock.lockForRead();
        payload = list.front();
        lock.unlock();
        return payload;
    }

    void add(T *p)
    {
        lock.lockForWrite();
        list.append(p);
        count = list.size();
        lock.unlock();
    }
    T *payload;
    quint32 count;
};

extern MavlinkPayloadManager<mavlink_attitude_t> mavlinkManagerAttitude;

#endif // MAVLINKDATAPROCESSOR_H
