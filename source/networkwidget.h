#ifndef NETWORKWIDGET_H
#define NETWORKWIDGET_H

#include <QWidget>
#include <QUdpSocket>
#include <QPushButton>
#include <QLabel>
#include <QtWidgets>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QSpacerItem>
#include <QLineEdit>
#include <QTextEdit>
#include <QObject>
#include <QThread>
#include "topbuttons.h"

class NetworkWidget : public QWidget
{
    Q_OBJECT
public:
    ~NetworkWidget();
    static NetworkWidget& getInstance(){
        if(NULL == instance){
            mutex.lock();
            if(NULL == instance){
                instance = new NetworkWidget();
            }
            mutex.unlock();
        }
        return *instance;
    }

    static void release(){
        if(instance != NULL){
            mutex.lock();
            delete instance;
            instance = NULL;
            mutex.unlock();
        }
    }

    QVBoxLayout *vBoxLayoutPage;
    TopButtons *topButtons;
    QSpacerItem *spacerUnderTopButtons;
    QHBoxLayout *hBoxLayoutUdpPortSetting;
    QLabel *labelUdpPortSetting;
    QPushButton *buttonUdpPortSetting;
    QLineEdit *lineEditUdpPortSetting;
    QTextEdit *textEditUdpReceivedData;

    QUdpSocket *udpSocket;
    QThread dataProcessThread;

    QHostAddress udpRemoteAddress;
    quint16 udpRemotePort;
    qint64 udpReceivedTotalLength;

private:
    explicit NetworkWidget(QWidget *parent = nullptr);
    NetworkWidget(const NetworkWidget&other); //拷贝构造函数
    NetworkWidget& operator=(const NetworkWidget&other); //赋值运算操作符

    static QMutex mutex;
    static NetworkWidget* instance;

private slots:
    void setUdpPort();
    void readUdpData();
    void updateUdpReceivedTextEdit(QString string);

signals:
    void startProcessData();
};

#endif // NETWORKWIDGET_H
