#ifndef TOPBUTTONS_H
#define TOPBUTTONS_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QDebug>
#include <QSpacerItem>
#include "mainwindow.h"
#include "doubleclickablebutton.h"

class TopButtons : public QHBoxLayout
{
    Q_OBJECT
public:
    TopButtons(QString stringOfWindow);
    int connectButtonsToMainWindow(MainWindow *mainWindow);
    int disconnectButtonsToMainWindow();
    QStringList stringList;
    QSpacerItem *spacerLeft;
    QSpacerItem *spacerRight;
    DoubleclickableButton *pushButtonHome;
    DoubleclickableButton *pushButtonStatus;
    DoubleclickableButton *pushButtonChart;
    DoubleclickableButton *pushButtonFCConfig;
    DoubleclickableButton *pushButton2DVision;
    DoubleclickableButton *pushButton3DView;
    DoubleclickableButton *pushButtonRC;
    DoubleclickableButton *pushButtonSerial;
    DoubleclickableButton *pushButtonNetwork;
    DoubleclickableButton *pushButtonSettings;

signals:

};

#endif // TOPBUTTONS_H
