#ifndef RCWIDGET_H
#define RCWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QtWidgets>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QSpacerItem>
#include "mainwindow.h"
#include "topbuttons.h"

class RcWidget : public QWidget
{
    Q_OBJECT
public:
    ~RcWidget();
    static RcWidget& getInstance(){
        if(NULL == instance){
            mutex.lock();
            if(NULL == instance){
                instance = new RcWidget();
            }
            mutex.unlock();
        }
        return *instance;
    }

    static void release(){
        if(instance != NULL){
            mutex.lock();
            delete instance;
            instance = NULL;
            mutex.unlock();
        }
    }

    QVBoxLayout *vBoxLayoutPage;
    TopButtons *topButtons;
    QSpacerItem *spacerUnderTopButtons;

private:
    explicit RcWidget(QWidget *parent = nullptr);
    RcWidget(const RcWidget&other); //拷贝构造函数
    RcWidget& operator=(const RcWidget&other); //赋值运算操作符

    static QMutex mutex;
    static RcWidget* instance;

signals:

};

#endif // RCWIDGET_H
