#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H
#include <QMutex>
#include <QSettings>
#include <QDebug>

class SettingsManager
{
public:
    ~SettingsManager();
    static SettingsManager& getInstance(){
        if(NULL == instance){
            mutex.lock();
            if(NULL == instance){
                instance = new SettingsManager();
            }
            mutex.unlock();
        }
        return *instance;
    }

    static void release(){
        if(instance != NULL){
            mutex.lock();
            delete instance;
            instance = NULL;
            mutex.unlock();
        }
    }
    void setValue(const QString key, const QString value);
    QString getValue(const QString key);
    int setConfigFile(const QString path);

    const QString defaultConfigFile = "default.ini";
    const QString keyConfigFile = "/APP/CONFIG_FILE";
    const QString keyModelFile = "/STATUS_PAGE/MODEL_FILE";
    QString configFileName;

private:
    explicit SettingsManager();
    SettingsManager(const SettingsManager&other); //拷贝构造函数
    SettingsManager& operator=(const SettingsManager&other); //赋值运算操作符

    static QMutex mutex;
    static SettingsManager* instance;

    QSettings *configIniFile;
    QString pathPrefix;
    bool useDefaultConfigFile;
};

#endif // SETTINGSMANAGER_H
