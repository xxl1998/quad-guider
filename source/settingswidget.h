#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QtWidgets>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QSpacerItem>
#include <QFontDatabase>
#include <QLineEdit>
#include "mainwindow.h"
#include "topbuttons.h"
#include "version.h"

class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    ~SettingsWidget();
    static SettingsWidget& getInstance(){
        if(NULL == instance){
            mutex.lock();
            if(NULL == instance){
                instance = new SettingsWidget();
            }
            mutex.unlock();
        }
        return *instance;
    }

    static void release(){
        if(instance != NULL){
            mutex.lock();
            delete instance;
            instance = NULL;
            mutex.unlock();
        }
    }

    QVBoxLayout *vBoxLayoutPage;
    TopButtons *topButtons;
    QSpacerItem *spacerUnderTopButtons;
    QLabel *labelProgramVersion;
    QHBoxLayout *hBoxLayoutConfigFileSetting;
    QLabel *labelConfigFileSetting;
    QPushButton *buttonConfigFileSetting;
    QLineEdit *lineEditConfigFileSetting;

private:
    explicit SettingsWidget(QWidget *parent = nullptr);
    SettingsWidget(const SettingsWidget&other); //拷贝构造函数
    SettingsWidget& operator=(const SettingsWidget&other); //赋值运算操作符

    static QMutex mutex;
    static SettingsWidget* instance;

private slots:
    void setConfigFile();

signals:

};

#endif // SETTINGSWIDGET_H
