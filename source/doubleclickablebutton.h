#ifndef DOUBLECLICKABLEBUTTON_H
#define DOUBLECLICKABLEBUTTON_H
#include <QPushButton>
#include <QTimer>

class DoubleclickableButton : public QPushButton
{
    Q_OBJECT

public:
    explicit DoubleclickableButton(QWidget *parent=nullptr);

signals:
    void singleClicked();
    void doubleClicked();

private slots:
    void slotTimerTimeOut();
    void slotClicked();

private:
    bool m_clicked;
    QTimer *m_timer;
};

#endif // DOUBLECLICKABLEBUTTON_H
