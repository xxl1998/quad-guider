#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QStatusBar>
#include <QtWidgets>
#include <QRadioButton>
#include <QUdpSocket>
#include <common/mavlink.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QWidget *mainWindowCentralWidget;
    QVBoxLayout *vBoxLayoutCentralWidget;

    QRadioButton * radioButtonSend;

    QUdpSocket *udpSocket;
    quint16 udpPort;

    QStatusBar *windowStatusBar;
    int pixelSize;
    QLabel *labelStatusHostPlatform;
    QLabel *labelStatusUDPSpeed;
    void updateLabelUdpSpeed(quint64 speedBytes);
    QLabel *labelStatusHeartbeat;
    quint32 heartbeatCount;
    void updateLabelHeartbeat(quint32 count);
    QLabel *labelStatusPackages;
    quint32 packagesCount;
    void updateLabelPackages(quint32 count);

    QTimer *timer;
    const int timerLoopTime = 10;
    quint32 bootTime;
    void sendLoop();
    mavlink_message_t message;
    quint8 sendBuffer[512];
    quint16 sendSize;
    quint64 sendSizeTotal;
    void mavlinkGenerate(quint32 msgid);

private:

protected:
    virtual void resizeEvent(QResizeEvent *event)override;

private slots:
    void changeUdpSendState();

};
#endif // MAINWINDOW_H
