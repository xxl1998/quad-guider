# MavLink消息发送程序

此程序用于测试上位机程序的MavLink相关功能。

QGC收到PX4 SITL在14550端口的MavLink消息：

```
30 31 32 33 36 83 85 --50Hz
74 --4Hz
291 --2Hz
0 1 8 24 141 245 --1Hz
147 230 242 340 --0.5Hz
4 241 --0.1Hz
```

共21种消息，按频率看有50Hz, 4Hz, 2Hz, 1Hz, 0.5Hz, 0.1Hz共6种，按发送周期看是20ms, 250ms, 500ms, 1000ms, 2000ms, 10000ms。

所以定时器周期设置为10ms，当达到发送周期时就通过UDP发数据。

## 技术要点

得益于前面积累的经验，没有遇到什么问题，MavLink的打包发送有官方的example做参考，具体打包发送过程如下：

```C++
#include <common/mavlink.h>

mavlink_altitude_t altitude;// 141
altitude.time_usec = 0;
altitude.altitude_monotonic = 487.35;
altitude.altitude_amsl = 488.105;
altitude.altitude_local = 0.06822;
altitude.altitude_relative = 0.052;
altitude.altitude_terrain = -0.032;
altitude.bottom_clearance = 0.1;
mavlink_msg_altitude_encode(1, 1, &message, &altitude);
sendSize = mavlink_msg_to_send_buffer(sendBuffer, &message);
```

首先创建对应的结构体，向里面写入数据，然后调用对应的encode函数将结构体数据更新到message中，最后调用mavlink_msg_to_send_buffer将message串行化变成发送buffer，并获取buffer实际大小。

使用UDP发送时，传入buffer地址和buffer实际使用的长度就行了。

## 已知问题

刚启动时50Hz的消息频率正常，运行一段时间后，变成33Hz左右，这个问题不影响使用，暂时先不管了。

## 本次提交信息

本次提交hash值：37af9ab91ba2a94f63aec2a20503edb30e293bc2

### 新增功能

此程序用于辅助调试主程序，可以模拟飞控，通过UDP发送MavLink数据包。

### 操作方式

启动程序，无需额外操作就会发送MavLink数据包到UDP14550端口。点击按钮可以停止/继续发送。

启动quad-guider程序，操作方式与使用PX4 SITL时一致。

### 预期效果

此程序状态栏可以显示发送的实时速度、数据包总数量、心跳包数量。

QGC可以自动连接，但此程序不会响应QGC发送的消息。

使用QGC的MavLink inspector可以显示此程序发送的MavLink数据包。

### 依赖

依赖qt的GUI库、网络库；依赖MavLink库。