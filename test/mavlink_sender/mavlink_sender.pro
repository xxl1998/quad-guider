QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DESTDIR = "$$PWD/../../executable"

SOURCES += \
    main.cpp \
    mainwindow.cpp \

HEADERS += \
    mainwindow.h

# mavlink
INCLUDEPATH+=$$PWD/../../3rd_party/mavlink_c_library_v2
INCLUDEPATH+=$$PWD/../../3rd_party/mavlink_c_library_v2/common


QMAKE_SPEC_T = $$[QMAKE_SPEC]
message(QMAKE_SPEC:$${QMAKE_SPEC_T})
message(QT_ARCH:$${QT_ARCH})
message(QMAKE_HOST.arch:$$QMAKE_HOST.arch)
message(QMAKE_HOST.os:$$QMAKE_HOST.os)
message(QMAKE_HOST.cpu_count:$$QMAKE_HOST.cpu_count)
message(QMAKE_HOST.name:$$QMAKE_HOST.name)
message(QMAKE_HOST.version:$$QMAKE_HOST.version)
message(QMAKE_HOST.version_string:$$QMAKE_HOST.version_string)

isEqual(QMAKE_HOST.os, Windows):{
    DEFINES += __HOST_PLATFORM_WINDOWS_
    message("define __HOST_PLATFORM_WINDOWS_")
}

isEqual(QMAKE_HOST.os, Linux):{
    DEFINES += __HOST_PLATFORM_LINUX_
    message("define __HOST_PLATFORM_LINUX_")
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
