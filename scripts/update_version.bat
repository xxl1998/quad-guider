@echo off
setlocal ENABLEDELAYEDEXPANSION

echo update version string with git commit id

::�����Ƿ����git�ֿ�
git rev-parse --show-cdup >nul 2>nul
if %errorlevel% == 0 (
	echo find git repo
	for /F %%i in ('git rev-parse --verify --short HEAD') do ( set commitid=%%i)
	echo commitid=!commitid!
	
	for /F %%i in ('git --no-optional-locks status -uno --porcelain') do ( set test_dirty=%%i)
	echo test_dirty=!test_dirty!
	if "!test_dirty!" == "" (
		echo git working tree clean
		set dirty_str=
	) else (
		echo git working tree dirty
		set dirty_str=-dirty
	)
	set hash_str=!commitid!!dirty_str!
 ) else (
 	echo no git repo
 	set hash_str=+
 )
 echo hash_str=!hash_str!
 echo current path: %~dp0
 set find_str=VERSION_HASH
 set file_name=%~dp0..\source\version_.h
 echo file path: %file_name%
 set file_new=%~dp0..\source\version.h

 For /f "tokens=1* delims=:" %%i in ('Type %file_name%^|Findstr /n %find_str%') do (
	set n=&set /a n=%%i+1
	echo will replace line:!n!
)
if "!n!" == "" (
	echo no string match
	::pause
	exit
)

(FOR /f "tokens=1*delims=:" %%a IN ('findstr /n "^" "%file_name%"') DO (
	SET "Line=%%b"
	IF %%a equ !n! SET "Line="!hash_str!""
	SETLOCAL ENABLEDELAYEDEXPANSION
	ECHO(!Line!
	ENDLOCAL
))>%file_new%
type %file_new%

::pause