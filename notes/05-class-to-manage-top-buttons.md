# 05 使用类管理顶部切换界面的按钮

## 技术要点

### 问题原因

前面在写界面的类的时候，就感觉这几个界面有通用的部分（也就是顶部切换界面的按钮）应该抽象出来，所以这里就把界面顶部的切换界面的按钮抽象为一个类来管理。

如果不这样做的话，每个界面的类里面都要分别创建所有的按钮，再各自配置属性，布局等等，会出现大量重复代码段，也容易出错。而且目前暂时规划的有10个界面：

- 主界面：回传图像、侧边图标、状态数据
- 状态界面：主要数据和3D飞机模型
- 曲线界面：曲线绘制
- 调参界面：飞控PID调节
- 2D视觉界面：图像回传、分类、目标跟踪等
- 3D视图界面：3D位置显示、空间轨迹飞行等
- 控制界面：云台控制、飞控控制（模拟遥控）
- 串口调试界面：串口设置、数据包配置
- 网络调试界面：TCP、UDP配置、数据包配置
- 设置界面：上位机配置、版本信息、版权信息

如果每个界面都写一遍，简直噩梦。

另外还有一个问题就是，这些button要绑定到mainwindow的槽函数上面，也要做解耦，否则又是大片的重复代码段。

### 实现方法

把按钮全都放到一个类里面比较顺利，新建了类TopButtons直接继承了QHBoxLayout，能够正常显示。

在connect解耦时遇到了问题，要把connect解耦就要把connect全都放在新的类TopButtons里面。这样有两种方案：（1）槽函数放在MainWindow类里面；（2）槽函数放在TopButtons类里面。

（1）connect传参需要mainwindow的指针和其中的槽函数的指针，mainwindow的指针容易获取，但是在使用变量传参时（槽函数和接收的对象mainwindow都使用变量传参），无法通过代码检查，编译失败，但是又不想使用qt4的传统方法，所以干脆只传mainwindow的指针了，槽函数直接写MainWindow类里面的方法。

（2）这种的话要在TopButtons类里面访问mainwindow里的widget，对其进行操作，耦合严重，不使用。

## 相关信息

### 本次提交信息

本次提交hash值：6482b66e2c2edeba70c2839eb851788eb19a3b6c

### 功能

使用类TopButtons管理顶部按钮。

### 操作方式

编译运行即可。

### 预期效果

能够正常显示按钮，点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

### 依赖

不依赖opencv。