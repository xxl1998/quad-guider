# 06 完善主界面

## 技术要点

### 设计目标

主界面最上面是切换界面的按钮，中间显示回传的图像，两边显示重要数据（pitch、roll、yaw、位置、速度等等），下面显示重要曲线（pitch、roll、yaw、位置、速度等等）。这些东西都叠加在回传图像上面，并半透明显示。

目前实现的功能：顶部按钮、中间label、图片叠加在最下层；

待实现：按钮半透明、label半透明、背景透明的曲线。

### 实现过程

遇到的第一个问题是，手动用代码布局的控件，会有很大的间距，导致上面放不下10个按钮，需要对QVBoxLayout（QHBoxLayout）调用方法setMargin去掉因水平/垂直布局带来的间距：

```
vBoxLayoutLabel->setMargin(0);
```

管理顶部按钮的类TopButtons继承的是QHBoxLayout，也需要调用setMargin。

```
    this->setMargin(0);
    this->setSpacing(0);
```

添加弹簧控件时，需要调用addSpacerItem：

```
vBoxLayoutLabel->addSpacerItem(spacerUnderTopButtons);
```

添加按钮、label等，需要调用addWidget：

```
vBoxLayoutLabelsLeft->addWidget(labelsLeft0);
```

添加layout，需要调用addLayout：

```
vBoxLayoutLabel->addLayout(hBoxLayoutLabels);
```

顶部按钮的大小，如果不进行额外设置的话会自动调整到比字最多的那个按钮大一圈，但是这样宽度就超过1280了，所以手动获取了字数最多的按钮的字体所占的宽度，并设置按钮的最小宽度，这样就能够达到1280的宽度了。调试过程需要注意，在mainwindow运行起来后，程序跑过了`w.show();`之后，才能获取到按钮和窗口的实际大小。

中间还遇到一个问题，编译成功，运行就crash（crash之前窗口正常显示了图片，只是无响应），把新增代码都注释完了，发现在homewidget.h中，public成员变量声明的顺序改变就crash（只声明，创建对象的代码注释掉了），百思不得其解，重新编译整个工程问题消失，猜测可能是改变头文件后某些文件没有自动编译？？

## 相关信息

### 本次提交信息

本次提交hash值：400c34f7c08ca7e3f900e8c9358cfe8292c7a1da

### 功能

完善主界面，完善顶部按钮，添加中间label，添加叠加在最下层的回传图像/静态图片；

### 操作方式

编译运行即可。

### 预期效果

能够正常显示按钮，能够正常显示左右两侧的label，能够正常显示叠加在最底层的图片。

点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

### 依赖

不依赖opencv。