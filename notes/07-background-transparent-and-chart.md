# 07 label背景半透明+添加曲线

## 技术要点

### 改动

仔细想了一下，还是把按钮不叠加在图像上了，因为按钮在每个界面都有，所以还是不搞半透明了。

### 按钮样式

目前收到的实现方法都是用stylesheet的，但是配置样式表后，很多属性都跟着变了，例如配置了按钮的边框颜色，那么它的大小就不是之前的大小了，还要手动调整一下里面的各种间距。

目前使用的按钮的样式表如下：

```
    QString stringStyleSHeet = "QPushButton{border-radius: 4px;padding:8px 0px;border: 2px solid #2D8DFF;"
        "background-color: white;}"
        "QPushButton:hover {background-color: #7ED1FF;color: white;}"
        "QPushButton:pressed {background-color: #2D8DFF;color: white;}"
        "QPushButton:disabled {background-color: rgba(45,141,255,100%);color: white;}";
```

配置了边框的颜色和粗细（border: 2px solid #2D8DFF;），文本到边框的距离（padding:8px 0px;）、边框圆角（border-radius: 4px;）、背景颜色（background-color: white;）、鼠标悬停在按钮上时的背景色和文本颜色（QPushButton:hover {background-color: #7ED1FF;color: white;}）、按下后的背景色和文本颜色、disable后的背景色和文本颜色。

另外把TopButtons放到和labelVideo同级的布局之后，这个布局也要设置spacing为0：

```
    vBoxLayoutPage->setMargin(0);// 每一个layout都要设置间距为0
    vBoxLayoutPage->setSpacing(0);
```

整个控件半透明的方法：

```
    auto effect = new QGraphicsOpacityEffect(this);
    effect->setOpacity(0.5);
    pushButton->setGraphicsEffect(effect);
    pushButton->setAutoFillBackground(true);
```

### 添加曲线

首先在工程文件.pro中添加charts支持：

```
QT       += charts
```

然后添加相关头文件：

```
#include <QLineSeries>
#include <QFontDatabase>
#include <QChart>
#include "qchartview.h"
```

一般来说还需要声明name space（使用`QT_CHARTS_USE_NAMESPACE`），但是后面要用opencv，为了避免可能出现的冲突，需要name space的地方一律手动指定。

之前是按照大部分教程里面说的，添加一个QGraphicsView然后在提升为QChartView，但是搜了一圈发现没有用代码提升的，所以就直接在布局里面添加了QChartView，好在可以显示。

```c++
// 声明：（头文件中）
    QtCharts::QChartView *chartView;
    QtCharts::QChart *chart;
    QtCharts::QLineSeries *lineSeries0;

// 创建对象
    chartView = new QtCharts::QChartView;
    lineSeries0 = new QtCharts::QLineSeries();// 曲线的数据
    chart = new QtCharts::QChart();

    // 添加数据
    lineSeries0->append(0, 6);
    lineSeries0->append(2, 4);
    lineSeries0->append(3, 8);
    lineSeries0->append(7, 4);
    lineSeries0->append(10, 5);
    *lineSeries0 << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);
    // 曲线
    chart->legend()->hide();
    chart->addSeries(lineSeries0);
    chart->createDefaultAxes();
//    chart->setTitle("Chart of AutoPilot data");
    chart->setBackgroundVisible(false);
    chart->setContentsMargins(0,0,0,0);
    chart->setMargins(QMargins(0,0,0,0));
    chart->layout()->setContentsMargins(0,0,0,0);
    // 显示曲线
    chartView->setChart(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setStyleSheet("background: transparent;border:0px;padding:0px 0px;");
    chartView->setMaximumHeight(labelVideo->height() * 0.3);
    vBoxLayoutLabel->addWidget(chartView);
```

这里把最大宽度限制到了图像的0.3倍，保证不会占据到窗口中间的位置。

在曲线能够正常显示之后发现曲线下面还有很大一片空白，尝试了stylesheet不行，看了一圈，layout里面都把间距去掉了，然后又跑了一下之前用qt creator布局的验证程序，发现之前也是有空白的，后来搜索发现，原来是QChart的对象要设置里面的margins，对QChartView操作是不行的。

曲线（实际上是折线）添加图例（qt里面叫做legend）：

```
    chart->legend()->setVisible(true);
    chart->legend()->detachFromChart();
    chart->legend()->setBackgroundVisible(false);
    int legendFontSize = chart->legend()->font().pointSize();
    qDebug() << "legendFontSize:" << legendFontSize;
    chart->legend()->layout()->setContentsMargins(0, 0, 0, 0);
    chart->legend()->setGeometry(QRectF(legendFontSize * 6, 0, 150, legendFontSize*10));
    chart->legend()->setAlignment(Qt::AlignTop);
```

这里因为pixelSize读出来是-1，所以用的pointSize，遇到笔记本的屏幕缩放可能会有问题。。。

### 窗口背景色

在调试时发现窗口背景色是灰色的，和整体界面不太搭，就把它换成白色的了，状态栏的背景和没有控件的空白处都变成白色，显示效果更加统一。

```
    // 窗口背景色
    QColor color = QColor(Qt::white);
    QPalette p = this->palette();
    p.setColor(QPalette::Window,color);
    this->setPalette(p);
```

### Ubuntu16给qt安装charts功能

下载源码，切到release版本，然后qmake、编译、安装：

```
git clone https://github.com/qtproject/qtcharts.git
cd qtcharts
git checkout v5.7.0
qmake
make -j2
sudo make install
```

## 相关信息

### 本次提交信息

本次提交hash值：56084853b8663429e3e4cdbc58ef6191f0101f68

### 功能

完善主界面的label的外观，完善顶部按钮的外观，添加背景透明的曲线。

### 操作方式

编译运行即可。

### 预期效果

主界面：能够正常显示按钮，能够正常显示左右两侧的label，能够正常显示叠加在最底层的图片，能够正常显示背景透明的曲线。按钮外观：蓝色边框，黑色字体，鼠标悬停时改变颜色，代表当前窗口的按钮是disabled状态，显示明显的蓝色。

点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

### 依赖

qt需要charts，对于Ubuntu来说可能需要使用源码编译安装。