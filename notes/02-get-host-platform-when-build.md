# 02 编译时获取主机系统

## 技术要点

### qt工程文件.pro中获取主机信息

查找qt官方文档，得知可以在qmake阶段获取到当前编译主机的信息：

https://doc.qt.io/qt-5/qmake-variable-reference.html#qmake-host

stackoverflow上面的讨论：

https://stackoverflow.com/questions/25916619/qmake-how-to-figure-out-host-platform-and-target-platform-when-cross-compiling

关于pro文件中添加条件判断语句的方法：

https://doc.qt.io/qt-5/qmake-test-function-reference.html

关于pro文件中添加宏定义的方法：

https://doc.qt.io/qt-5/qmake-variable-reference.html#defines

### 实现方法

在pro文件中做如下定义：

```
QMAKE_SPEC_T = $$[QMAKE_SPEC]
message(QMAKE_SPEC:$${QMAKE_SPEC_T})
message(QT_ARCH:$${QT_ARCH})
message(QMAKE_HOST.arch:$$QMAKE_HOST.arch)
message(QMAKE_HOST.os:$$QMAKE_HOST.os)
message(QMAKE_HOST.cpu_count:$$QMAKE_HOST.cpu_count)
message(QMAKE_HOST.name:$$QMAKE_HOST.name)
message(QMAKE_HOST.version:$$QMAKE_HOST.version)
message(QMAKE_HOST.version_string:$$QMAKE_HOST.version_string)

isEqual(QMAKE_HOST.os, Windows):{
    DEFINES += __HOST_PLATFORM_WINDOWS_
    message("define __HOST_PLATFORM_WINDOWS_")
    INCLUDEPATH+=C:\Qt\opencv_build\install\include
    LIBS+=C:\Qt\opencv_build\lib\libopencv_*.a
}

isEqual(QMAKE_HOST.os, Linux):{
    DEFINES += __HOST_PLATFORM_LINUX_
    message("define __HOST_PLATFORM_LINUX_")
    INCLUDEPATH += /usr/local/include \
                    /usr/local/include/opencv \
                    /usr/local/include/opencv4
    LIBS += /usr/local/lib/libopencv_*
}
```

在不同的分支中引用了不同的库和头文件路径，可以兼容windows和Linux。

打印输出的语法与shell脚本比较相似，其他的也都很容易理解，主要就是根据不同的系统定义不同的宏，之后在代码中进行条件编译。

奇怪的是，这些message会被输出三遍，可能是qt creator的编译过程执行了多次qmake？？

打印信息如下：

```
Project MESSAGE: QMAKE_SPEC:win32-g++
Project MESSAGE: QT_ARCH:i386
Project MESSAGE: QMAKE_HOST.arch:x86
Project MESSAGE: QMAKE_HOST.os:Windows
Project MESSAGE: QMAKE_HOST.cpu_count:16
Project MESSAGE: QMAKE_HOST.name:XXL-PC
Project MESSAGE: QMAKE_HOST.version:10.0.19041
Project MESSAGE: QMAKE_HOST.version_string:10
Project MESSAGE: define __HOST_PLATFORM_WINDOWS_
```

系统是64位的，但是qt是32位的。

之后在UI中添加一个label，在代码中设置label的文字：

```cpp
#ifdef __HOST_PLATFORM_WINDOWS_
    ui->label_2->setText(QString("Host windows"));
    ui->label_2->adjustSize();
#elif defined __HOST_PLATFORM_LINUX_
    ui->label_2->setText(QString("Host linux"));
    ui->label_2->adjustSize();
#else
    ui->label_2->setText(QString("Host unknown"));
    ui->label_2->adjustSize();
#endif
```

发现一个bug，只修改pro文件不修改源码，不会重新编译代码。


## 相关信息

### 本次提交信息

本次提交hash值：c7caceaa0a9bfaf19b8e482800ef256bab9a3ddd

### 功能

编译时获取主机系统信息，通过条件编译方式使程序进入不同分支，达到一份代码兼容多平台的目的。

### 操作方式

编译运行即可。

### 预期效果

图片下方字符串中内容，与当前系统一致。

### 依赖

依赖自行编译的opencv库，参考xxx文章。（TODO）