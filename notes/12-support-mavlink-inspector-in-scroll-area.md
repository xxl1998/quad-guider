# 12 支持Mavlink监视器

**此项目中添加了一个测试程序mavlink_sender用于模拟飞控发送的UDP数据包，可以用于测试quad-guider主程序。**

## 技术要点

本次主要是实现MavLink监视器，可以显示已经收到的所有类型的MavLink消息，并且能够动态地清空、更新。

上次已经大概研究了数据接收、解析、更新UI的逻辑，在具体实现时遇到的问题主要是界面需要动态更新（控件/布局动态增减、控件内容动态刷新），而且有些控件不需要频繁更新（主界面label），有些控件则需要接收全部采样数据（曲线绘制），还需要处理好信号的连接，要能把其他界面的槽函数连接到解析数据后发送的信号（代码耦合的问题）。

需要动态更新界面（控件/布局动态增减）的主要是本次实现的MavLink监视器和飞控参数配置功能，而且它们两个都有共同之处，区别就是前者是飞控主动上报的数据，后者是上位机查询了飞控的全部参数（或指定的部分参数）后更新的，后者涉及到了写的操作。

为了简化信号的连接，设计以下信号（数据解析完成后发送的）：

```C++
void messageReady(mavlink_message_t msg);
```

UDP在解析到一个完整的MavLink数据包后就会发送此信号，将整个message传过去，供MavLink监视器更新UI。

### 滚动区域的实现

滚动区域通过QScrollArea类实现，需要给QScrollArea一个容器类，这里容器是用的QWidget，然后给容器设置layout，在layout里面添加控件，再设置容器类的大小，使其超过QScrollArea的大小，就会出现滚动条了。

要动态更新的话，需要容器自动调整大小，并且QScrollArea每次也要确认新添加的控件可视。

```C++
    if(insertPosition != -1){
        vBoxLayoutMavlinkMessages->insertWidget(insertPosition, mavlinkMessage);
    }else{
        vBoxLayoutMavlinkMessages->addWidget(mavlinkMessage);
    }
    connect(mavlinkMessage, &QPushButton::clicked,this, &StatusWidget::updateMavlinkMessageDetailLayout);
    scrollAreaContentMavlinkMessages->adjustSize();
    scrollAreaMavlinkMessages->ensureWidgetVisible(mavlinkMessage);
```

每次添加后调用ensureWidgetVisible保证新添加到控件可见。

### 获取MavLink message的字符串

在文档里面没找到，代码里面倒是找到了宏定义，但是不知道怎么使用，官方的example里面也没有使用字符串，最后还是在QGC的代码里面找到了相关用法，所在文件：`src\AnalyzeView\MAVLinkInspectorController.cc`，在QGCMAVLinkMessage的构造函数里面。

具体使用方法如下：

```C++
#define MAVLINK_USE_MESSAGE_INFO
#include <common/mavlink.h>

const mavlink_message_info_t* msgInfo = mavlink_get_message_info(msg);
if (!msgInfo) {
    qDebug() << QStringLiteral("MAVLinkMessage NULL msgInfo msgid(%1)").arg(msg->msgid);
    return;
}
qDebug() << "New Message:" << msgInfo->name;
```

在引用mavlink.h之前要定义MAVLINK_USE_MESSAGE_INFO，才能使用。

### 动态刷新界面

到这里时仔细想了一下，主要需要解决的是实时性(接收时解析)、大数据量（保存/解析文件）两个问题。

功能上面，需要支持随时断开（或重新连接）UDP（或串口），随时暂停（或恢复）接收（或显示），此外，UDP与串口是不同的数据通路，应做互斥处理（防止两个数据源的MavLink消息显示时混在一起），暂时只支持一个MavLink数据流吧。

MavLink监视器是需要最新的消息列表、消息内容、刷新率。绘制曲线才需要保存收到的全部数据。所以本次实现的MavLink监视器获取数据通过qt信号-槽机制传入整个mavlink_message_t，不保存旧数据。

首先添加mavlinkdataprocessor.cpp文件，将processData函数从头文件中挪过去。

将状态页面的布局修改一下，左边显示MavLink消息列表，右上显示MavLink消息详细内容，右下显示飞机3D模型。

添加类MavlinkMessage，对应每一个MavLink消息，用于显示消息列表。

每一个消息都使用mavlink_message_t保存和显示最新的消息，并计算频率。

添加如下函数，用于MavLink消息的动态添加与更新。

```
    void addMavlinkMessage(mavlink_message_t *msg);
    QWidget* findMavlinkMessage(quint8 componentId, quint32 messageId);
```

在动态刷新界面的时候发现，有些消息会出现多次，仔细调试发现是message id在传参过程中发生了变化，这是因为传的是指针，由于子线程循环速度很快，子线程里的那个变量很快被刷掉了。

解决方法就是把传的指针改成了直接传结构体。

在给MavLink消息排序时，需要用到字符串比较，由于C++的运算符重载，使用QString直接用大于号就比较了，这就比C优雅很多，只是要深入学习的话还是要下很大功夫。

### 支持显示消息详细内容/频率

添加类MavlinkMessageField，对应MavLink消息内容中的一个field，用于显示消息详细内容。

updateMavlinkMessageDetailData用于更新MavLink消息详细内容，但并不直接调用，而是在收到新的消息后，判断是当前选中要显示的消息，先启动50ms的定时器，定时器计时期间仍然可以继续接收新的消息，定时器时间到达后，使用最新的消息去更新UI。这样就限制了MavLink消息详细内容的最大刷新率为20Hz，防止UI变化太快。

调试的时候发现一个奇怪的问题，updateMavlinkMessageDetailData调用2次后，vBoxLayoutMessageDetail里面的内容只剩下一半。本来以为是线程同步的问题，但是这时是已经加了读写锁的，加锁的代码查了一下也没什么问题。后来定位到问题就在函数updateMavlinkMessageDetailData里，最终发现是获取控件的时候使用了`takeAt`（会把控件删掉），实际应该使用`itemAt`。

函数updateMavlinkMessageDetailData中更新字符串的代码来自QGC源码中`src\AnalyzeView\MAVLinkInspectorController.cc`中的`void QGCMAVLinkMessage::_updateFields(void)`。这部分代码的功能主要是类型的判断与转换、支持数组等。

消息频率的计算是通过MavlinkMessage类中保存的消息计数和之前两次收到消息的时间。定时器每秒触发一次，如果大于1Hz的消息，通过消息计数计算频率，小于1Hz的消息则通过两次收到消息的时间差计算。

使用了读写锁防止多处同时访问同一资源（控件或自定义类）。

### MavLink消息的保存（预研--用于曲线绘制功能）

目前来看，数据同步使用全局变量+读写锁比较合适。在MavlinkDataProcessor类中实现对全局变量的读写函数，然后在UI更新中调用来获得最新的MavLink消息（或当前接收的全部MavLink消息）。这样可以减少数据的复制。

全局变量保存的内容，如果使用mavlink_message_t可以保存全部信息，但数据量太大（结构体占296字节），假设每秒400个message，则10分钟的数据量就高达70MB左右，而UDP原始数据也不过平均每个数据包40字节左右，差了7倍，而假设大部分数据都是mavlink_attitude_t（占28字节），则差距10倍以上。所以不保存mavlink_message_t，而是将所有支持的消息类型分别保存。

MavLink是支持多机（sysid代表ID of message sender system/aircraft），每台无人机/机器人上面的多个模块（compid代表ID of the message sender component），目前的方案，通过面向对象化（支持动态分配/释放）理论上也可以支持，不过暂时还是只支持飞控的MavLink吧。

在开始写代码时突然发现，如果每种消息类型都写一个读写函数的话，代码将极其臃肿，如果为此简单地写一个类的话，就可以支持sysid、compid，也可以代码复用，但是创建链表的时候要用结构体指针，而且动态的结构体是否能作为构造函数的参数？----使用模板类可以实现，但是UI线程在使用数据时，代码的解耦似乎是个问题。（这部分代码写了一些，但还没使用）

## 已知问题

有时会出现scrollAreaContentMavlinkMessages里面的button高度异常。

有些地方的控件size是固定大小的，需要改成动态计算的。

## 本次提交信息

本次提交hash值：c7b06e53feef049c9811d7f3d2fb5ba598db84e0

### 新增功能

状态页面支持显示MavLink消息列表和消息详细内容，支持停止/继续刷新显示，支持显示消息的message id、刷新率。

### 操作方式

启动程序后，切换到网络界面，点击端口右边的set按钮启动UDP监听。

需要PX4 SITL或者其他的UDP的MavLink数据流。

监听UDP与启动PX4的顺序无要求，程序运行时关闭PX4不影响程序。

### 预期效果

主界面：能够正常显示按钮，能够正常显示左右两侧的label，能够正常显示叠加在最底层的图片，能够正常显示背景透明的曲线。按钮外观：蓝色边框，黑色字体，鼠标悬停时改变颜色，代表当前窗口的按钮是disabled状态，显示明显的蓝色。

点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

状态界面：能够显示彩色飞机3D模型，在模型上面拖动鼠标可以改变视角，鼠标右键拖动以另一种方式改变视角，鼠标滚轮可以整体缩放视角，鼠标释放后模型下面的label中更新观察模型的camera的位置。多次切换到状态界面，3D模型都能正常显示。**能够显示MavLink消息列表（可以正常更新各消息的刷新率），点击某一消息能够显示MavLink消息的详细内容。**

网络界面：点击set按钮，能够在文本框中显示出UDP收到的原始数据，主界面的label显示的姿态角能够正常更新。

设置界面：能够读取到当前使用的配置文件，可以在编辑区修改，点击按钮可以设置为新的配置文件，当文件不存在时会弹窗警告，设置成功时无提示或报错。编辑区只支持输入数字字母下划线。

分离窗口：双击顶部按钮可以分离出新窗口，所有窗口地位等同，都可以任意切换界面，关闭某个窗口不影响其他窗口。

### 依赖

qt需要charts，对于Ubuntu来说可能需要使用源码编译安装。

可配置是否使用assimp库，使用assimp库时需要OpenGL3.3，不使用assimp时无法显示彩色3D飞机模型。

**需要有PX4的MavLink数据流，可以使用此项目中包含的测试程序mavlink-sender模拟飞控发送的UDP数据包，也可以是源码编译的PX4 sitl或者连接了UDP数传的运行PX4的飞控。**