# 11 支持UDP&Mavlink获取飞控状态

## 技术要点

设计目标是兼容PX4的部分特性，类似于QGroundControl相关功能的子集。

因为自研飞控相对来说参数较少，使用QGC略为复杂，自己实现一遍也有利于Qt、MavLink等技术的提高。

目前以PX4 SITL为基础，实现MavLink协议的支持（部分特性）以及相关参数、数据在上位机上的显示。

本次暂时只实现基本的读取主要数据（姿态等）并显示，参数获取和参数设置后面再实现。

### MavLink的连接

经过实验，在VMware中的Ubuntu20上面使用PX4代码，使用命令`make px4_sitl gazebo`跑起来gazebo，在Ubuntu20中直接启动QGC无需额外设置就能自动连接SITL模式的PX4。

关于PX4开发环境的建立，可以看官方文档，一步步跟着做就能编译成功，但是需要能够流畅地使用github，这个是另一个坑，在其他的博客/文章里面再写吧。

**在Windows中的操作前提是虚拟机与Windows主机之间网络是通的，也就是可以互相ping通。**

#### QGC手动连接（需要IP和端口号）

在win11中启动QGC，需要手动配置连接，配置过程如下：

- 点击左上角QGC图标，选择“Application Settings”
- 左侧选择“Comm Links”，点击下方的“Add”
- Type选择“UDP”，在下面配置端口号14580
- 点击下面的Add，输入Ubuntu20的IP，点击右下角的“OK”
- 选中刚刚添加的连接，点击下面的“Connect”进行连接

连接成功后，QGC表现与在Ubuntu20中表现相同。

Ubuntu20的IP通过`ifconfig`命令查看。

端口号通过命令`sudo netstat -anp | grep px4`查看px4使用了哪些UDP的端口进行广播，我这里看到的有14280、14580、18570、13030。它们之间的区别主要是回传数据的种类和频率不同。配置文件是`./build/px4_sitl_default/etc/init.d-posix/px4-rc.mavlink`，具体可以研究PX4源码。

#### packetsender手动连接（需要IP和端口号）

packetsender下载链接：

https://packetsender.com/download

packetsender是一个跨平台的网络调试工具。这里用它来看一下UDP的原始数据，打开packetsender后，首先设置一下UDP Server，用于接收PX4的MavLink数据。设置方法如下：

- 点击左上角菜单栏“File”->“Settings”
- 默认就是network标签页，在“UDP Server Ports”右边的输入框中输入14030
- 确认上面的“Enable UDP Servers”是勾选的，点击右下角OK关闭设置

然后向PX4发送一个数据触发MavLink，步骤如下：

- 在packetsender的主界面的ASCII输入框中输入任意字符串，例如“a”
- 在Address输入框中输入Ubuntu的IP
- 在Port输入框中输入14280
- 选择通信协议为UDP，点击send按钮
- 触发成功，log窗口滚动刷新出收到的UDP数据包

可以看到UDP数据包都有相同的起始字节，数据包的长度从几十到几百字节不等。如果是0xFD开头的，表明是MavLink V2的协议；0xFE开头的是MavLink V1的协议。（默认是MavLink V1的，在QGC连接一次后，会变成MavLink V2的，下次启动PX4 sitl又变回MavLink V1的，应该是QGC自动配置了）

#### PX4广播-支持监听模式

默认编译的PX4 sitl，UDP不是广播模式的，只有localhost能够收到，Windows需要知道Ubuntu的IP才能连接。

在不用QGC连接时发现windows下收不到数据，查看PX4的log发现mavlink两次报这个广播的问题。

```
INFO  [mavlink] mode: Normal, data rate: 4000000 B/s on udp port 18570 remote port 14550
INFO  [mavlink] mode: Onboard, data rate: 4000000 B/s on udp port 14580 remote port 14540
INFO  [mavlink] mode: Onboard, data rate: 4000 B/s on udp port 14280 remote port 14030
INFO  [mavlink] mode: Gimbal, data rate: 400000 B/s on udp port 13030 remote port 13280
INFO  [logger] logger started (mode=all)
INFO  [logger] Start file log (type: full)
INFO  [logger] [logger] ./log/2022-10-05/08_15_11.ulg	
INFO  [logger] Opened full log file: ./log/2022-10-05/08_15_11.ulg
INFO  [mavlink] MAVLink only on localhost (set param MAV_{i}_BROADCAST = 1 to enable network)
INFO  [mavlink] MAVLink only on localhost (set param MAV_{i}_BROADCAST = 1 to enable network)
```

经过一通操作，在rcS里面配置MAV_1_BROADCAST没有生效。。。。又摸索了一下，发现在已知Ubuntu的IP时，向它的14280端口发任意数据，它就会把MavLink发到Windows，用packetsender可以接收。（也就是上文手动触发的方式）

又使用某英文搜索引擎搜了一下，才发现设置广播的方法，编辑这个文件：

```
cd ~/Desktop/PX4-Autopilot
vim ./build/px4_sitl_default/etc/init.d-posix/px4-rc.mavlink
```

在里面的`mavlink start`命令最后面添加`-p`，就能广播了，然后在Windows上面打开QGC，不需要任何额外操作就能自动连接。查看PX4文档（https://docs.px4.io/main/zh/simulation/）得知，QGC使用的是14550端口，这样使用packetsender监听14550端口可以达到同样效果，就不用手动输入Ubuntu的IP了。

### UDP通信

目前先支持监听14550端口的方式。

#### Qt支持UDP接收

首先在pro文件中添加network支持：

```
QT       += core gui network
```

同样新建一个网络界面的类NetworkWidget，依旧是单例模式，然后添加头文件支持UDP：

```
#include <QUdpSocket>
```

创建socket：

```
udpSocket = new QUdpSocket();
```

监听端口：

```
udpSocket->bind(QHostAddress::AnyIPv4,lineEditUdpPortSetting->text().toInt());
```

连接信号：

```
connect(udpSocket, &QUdpSocket::readyRead, this, &NetworkWidget::readUdpData);
```

在槽函数中读取数据：

```
readLength = udpSocket->readDatagram(array.data(),array.size(),&address,&port);
```

Qt已经封装的很好了，UDP通信在配置正确，网络通畅，防火墙没有拦截的情况下，没有什么问题。

而且UDP是基于数据包的，不像TCP是基于字节流的，UDP每次通信都是一个数据包，而TCP不保证发送端发送的次数以及每次发送的内容和接收端一致，TCP只保证数据不丢，数据顺序不变，至于接收几次以及每次接收多少数据它是不保证的。

所以TCP就有了所谓的“粘包”、“半包”问题。。即当发送方短时间内发了多次小数据量时，接收方**可能只触发了一次接收**就收到了全部数据，称为“粘包”；当发送方一次发了大量数据时，接收方**可能是分几次收到的**，称之为“半包”。知乎上面还有很激烈的讨论。。例如下面这段话：

```
【我们的意思是tcp是没有半包和粘包的概念，tcp->传输层->流式协议，不存在半包和粘包，半包和粘包是针对应用层或者应用层协议而言不能说成tcp半包粘包问题】，tcp内部的传输分包是tcp自己滑动窗口以及拥塞窗口控制的事情，并加上一些其它机制比如重传向外表现成的是一个流式协议，说tcp半包粘包是不专业的事情
```

。。。。我的观点是，TCP本身肯定是没有问题的，用了那么多年了，这个就**是TCP的特性**。但是，用户使用TCP是要解决这个问题的，**虽然不是TCP本身的问题，但是是用户面临的问题**，我觉得没必要对表述上纲上线，正如我说“STM32串口接收不定长数据问题”只会想到是要教用户写代码，而不是说芯片有问题。

不过好在本次是搞UDP的，而且MavLink本身就是基于数据包的，这个问题也就不是问题了。

#### 多线程+异步数据处理

由于UDP数据量很大，而且回调次数多，直接在槽函数中进行数据处理的话，UI更新会带来严重的卡顿，甚至导致程序卡死，所以需要对数据进行异步处理，在接收UDP数据的槽函数中保存数据，在另一个线程中去解析数据并更新UI。

保存数据的话，这里选择使用链表进行操作。（数据的特点：大量、不连续、动态分配、长度不定、按顺序存取）

在槽函数中接收UDP数据，然后将数据保存到链表中。主要代码如下：

```C++
    QByteArray *array = new QByteArray;
    qint64 readLength;

    //根据可读数据来设置空间大小
    array->resize(udpSocket->bytesAvailable());
    //读取数据
    readLength = udpSocket->readDatagram(array->data(),array->size(),&udpRemoteAddress,&udpRemotePort);
    udpReceivedTotalLength += readLength;
    array->resize(readLength);
    //加锁防止同时访问链表
    mutexListUdpReceivedData.lock();
    listUdpReceivedData.append(array);
    mutexListUdpReceivedData.unlock();
```

多线程的实现方式：新建一个类MavlinkDataProcessor继承QObject，类MavlinkDataProcessor中的槽函数processData就是子线程的函数，创建一个QThread对象，将MavlinkDataProcessor的对象使用moveToThread关联到QThread对象，连接相关信号（槽函数processData一定要连接），处理好线程结束的条件，处理好资源的释放。

然后编译时发现，继承QObject的类MavlinkDataProcessor放在networkwidget.cpp中编译报错，单独放在一个头文件里面才算编译过了。。。（实际上是Q_OBJECT放在cpp中导致的，放到头文件中就可以了，一组cpp和h文件里面可以定义多个类）

又发现发送信号时，链表作为参数没有传过去，注册类型后传过去了无法使用，无奈只能用全局变量访问链表了。

接着又发现，在线程里面读到的QByteArray的size不对，原因是在接收UDP数据之前resize了，resize的大小是UDP可读数据的大小（可能包含了多个数据包），而每次读取都只读了一个数据包，需要在UDP接收之后再次把QByteArray给resize成实际读取的长度。（这也表明，链表加锁导致的槽函数中的阻塞目前还不会导致UDP接收问题）

目前子线程是每次循环sleep 100ms，这样文本框的刷新频率低于10Hz，程序不会卡顿了。

到此数据接收和异步处理基本OK了，还有一些坑，就是很有可能还存在内存泄露，以及某些写法不太规范，这个慢慢迭代吧。

### 支持MavLink

官网：https://mavlink.io/zh/

直接就有现成的C库：https://github.com/mavlink/c_library_v2

C的教程：https://mavlink.io/zh/mavgen_c/

数据格式：https://mavlink.io/zh/guide/serialization.html

message汇总：https://mavlink.io/zh/messages/common.html#messages

这里直接从github下载压缩包，放到3rd_party目录，MavLink的实现有common、minimal、standard等，看了一下文档，就用common吧。。

因为一共400多个文件，占了接近9MB的空间，所以删掉了以下目录：

```
ardupilotmega
ASLUAV
icarous
matrixpilot
uAvionix
```

最终是留下275个文件，6.29MB。

在源码中引用头文件，并在pro文件中添加头文件路径：

```
INCLUDEPATH+=$$PWD/../3rd_party/mavlink_c_library_v2
INCLUDEPATH+=$$PWD/../3rd_party/mavlink_c_library_v2/common
```

然后在处理数据的线程里面解析MavLink数据包：

```C++
mavlink_status_t status;
mavlink_message_t msg;
mavlink_attitude_t attitude;
int chan = MAVLINK_COMM_0;
        
for(int i = 0; i < array->size(); i++)
{
	if (mavlink_parse_char(chan, array->at(i), &msg, &status))
	{
        qDebug("Received message with ID %d, sequence: %d from component %d of system %d\n",msg.msgid, msg.seq, msg.compid, msg.sysid);
        // ... DECODE THE MESSAGE PAYLOAD HERE ...
        switch(msg.msgid) {
        case MAVLINK_MSG_ID_ATTITUDE:
        {
            mavlink_msg_attitude_decode(&msg, &attitude);
            qDebug("pitch:%f roll:%f yaw:%f\n",
            attitude.pitch, attitude.roll, attitude.yaw);
        }
        break;
        }
	}
}
```

MavLink解析是每次传入一个字节，典型的嵌入式系统解析数据包的方法。

判断数据包类型的方法：首先根据数据格式找到当前数据包的message ID，然后在message汇总里面找到对应的message，再去头文件中找到对应的decode函数进行调用。

例如数据包：

```
FE 1C 1B 01 01 1E E0 53 00 00 DE 3E 71 BC 5E 9F 82 BC 1E 5B CE 3F A0 60 93 39 F8 7F F5 BA 56 08 50 BA 71 98 
```

根据（https://mavlink.io/zh/guide/serialization.html）由第一个字节0xFE知道是MavLink V1，message ID在第六个字节，是0x1E，也就是30，在（https://mavlink.io/zh/messages/common.html#messages）中搜索30，找到它是ATTITUDE（https://mavlink.io/zh/messages/common.html#ATTITUDE），对应的头文件是mavlink_msg_attitude.h，在头文件里面找到decode函数，就可以调用了。

到此MavLink就能正常使用了，整个接收、解析的数据通路完成，下面就是把解析出来的数据用于更新UI了。

### 更新UI

**性能与设计的考量：**

根据测试，默认编译的PX4 sitl，在14550端口，30秒发了10052个数据包，平均每个数据包都在40字节左右。用本项目的程序，在14550端口，30秒收到的数据量约为430KB，与估算值相符。

也就是说上位机每秒要处理335个数据包，每次循环100ms处理33个，与程序运行结果相符（每次循环处理36个左右）。考虑到自研飞控的数据回传频率，上位机应当能够至少每秒处理400个数据包。

此外，不同的UI组件的刷新率也要不同。

曲线的刷新率应当最高，至少保证30Hz的刷新率，平均刷新率应在50~60Hz，再高的话显示器一般达不到了，没有必要，更应保证的是不卡顿，不丢数据，能够将所有采样点显示在曲线上面。（因为是刷新的部分控件，并非整个窗口，也不是刷新的图像，所以这里不说FPS，而是用的Hz）

其他的label，刷新率不需要太高，只需要知道数据是否在正常范围内、是否正常更新就行了。

另外预计在状态界面添加MavLink消息查看器，也就是QGC里面的MavLink inspector。这个刷新率也不需要太高，但是要能够展示已收到的全部MavLink消息类型，需要缓存大量数据。还需要能够动态添加、查找、更新、清空里面的控件。

所以需要在子线程里面将解析出来的数据，根据消息类型的不同，通知不同的控件更新界面，需要传递多种不同的结构体。传参可以只传一个指针（void *），在槽函数里面去拿数据进行UI更新。不同控件的刷新率可以通过控制发送信号的频率来实现，需要把大循环的时间再调一下，满足曲线的刷新率要求。暂时也没想到别的好的办法，希望信号与槽的机制能达到性能要求吧。

**更新主界面的label：**

这次暂时只实现更新这几个label，HomeWidget类中添加槽函数：

```
public slots:
    void updateAttitude(void *p);
```

因为要在NetworkWidget中访问（连接信号），所以是public的。连接信号：

```
connect(mavlinkDataProcessor, &MavlinkDataProcessor::attitudeReady, &HomeWidget::getInstance(), &HomeWidget::updateAttitude);
```

MavlinkDataProcessor类中添加信号：

```
void attitudeReady(void *p);
```

在对应的message中发送信号：

```
case MAVLINK_MSG_ID_ATTITUDE:
{
	mavlink_msg_attitude_decode(&msg, &attitude);
// qDebug("pitch:%f roll:%f yaw:%f\n",
//     attitude.pitch, attitude.roll, attitude.yaw);
	emit attitudeReady(&attitude);
}
```

这个实现比较简单，就是需要注意传过来的姿态角是弧度制，需要手动转换成角度制，方便查看。

这里用的C风格的强制类型转换，应该也可以使用C++风格的指针转换。

## 本次提交信息

本次提交hash值：66fa9c0409c20e5451a607d96f03207ddec56538

### 新增功能

添加了网络界面，支持UDP接收，支持MavLink数据解析，支持主界面label显示飞控姿态。

### 操作方式

需要PX4 SITL或者其他的UDP的MavLink数据流。

监听UDP与启动PX4的顺序无要求，程序运行时关闭PX4不影响程序。

### 预期效果

主界面：能够正常显示按钮，能够正常显示左右两侧的label，能够正常显示叠加在最底层的图片，能够正常显示背景透明的曲线。按钮外观：蓝色边框，黑色字体，鼠标悬停时改变颜色，代表当前窗口的按钮是disabled状态，显示明显的蓝色。

点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

状态界面：能够显示彩色飞机3D模型，在模型上面拖动鼠标可以改变视角，鼠标右键拖动以另一种方式改变视角，鼠标滚轮可以整体缩放视角，鼠标释放后模型下面的label中更新观察模型的camera的位置。多次切换到状态界面，3D模型都能正常显示。

**网络界面：点击set按钮，能够在文本框中显示出UDP收到的原始数据，主界面的label显示的姿态角能够正常更新。**

设置界面：能够读取到当前使用的配置文件，可以在编辑区修改，点击按钮可以设置为新的配置文件，当文件不存在时会弹窗警告，设置成功时无提示或报错。编辑区只支持输入数字字母下划线。

分离窗口：双击顶部按钮可以分离出新窗口，所有窗口地位等同，都可以任意切换界面，关闭某个窗口不影响其他窗口。

### 依赖

qt需要charts，对于Ubuntu来说可能需要使用源码编译安装。

可配置是否使用assimp库，使用assimp库时需要OpenGL3.3，不使用assimp时无法显示彩色3D飞机模型。

**需要有PX4的MavLink数据流，可以是源码编译的PX4 sitl或者连接了UDP数传的运行PX4的飞控。**