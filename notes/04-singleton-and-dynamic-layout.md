# 04 各窗口单例模式+动态布局

## 技术要点

### 面向对象与单例模式

目前对面向对象编程的理解还非常粗浅，对C++更是只知皮毛。所以窗口的管理暂且只用单例模式，也就是把每个窗口都用不同的类来管理，然后每个窗口对应的类都只有一个对象/实例。后面在学习/进步的过程中再慢慢迭代。

### 单例模式的实现

单例模式主要实现要点就是让构造函数是private的，这样就无法通过构造函数去创建对象，只能通过类里面提供的获取实例的方法getInstance去获取到当前这个类的唯一的实例。

当然在实现过程中还要注意线程安全以及不要损失性能，这方面博客很多，就不展开了，但是在使用qt智能指针QScopedPointer时发现，我这个类是涉及到了界面和槽函数，导致空间被提前释放了，关闭程序的时候会crash，所以我使用手动释放的方式，把信号和槽函数断开，再手动释放。

具体实现如下：

```c++
class HomeWidget : public QWidget
{
    Q_OBJECT
public:
    ~HomeWidget();
    static HomeWidget& getInstance(){
        if(NULL == instance){
            mutex.lock();
            if(NULL == instance){
                instance = new HomeWidget();
            }
            mutex.unlock();
        }
        return *instance;
    }

    static void release(){
        if(instance != NULL){
            mutex.lock();
            delete instance;
            instance = NULL;
            mutex.unlock();
        }
    }

private:
    explicit HomeWidget(QWidget *parent = nullptr);
    HomeWidget(const HomeWidget&other); //拷贝构造函数
    HomeWidget& operator=(const HomeWidget&other); //赋值运算操作符

    static QMutex mutex;
    static HomeWidget* instance;

signals:

};
```

获取实例和释放的两个方法都在头文件中实现了，这样cpp文件中清爽一些。

这个类是用来管理整个窗口的所有控件的，所以是继承了QWidget。

### 动态布局

这个实现的过程比较曲折。因为qt本身不支持（对最外层窗口/widget）动态设置layout。当创建工程时使用QMainWindow时，本身它的layout就已经有了，不能再对自动生成的w这个对象来setLayout了，只能setCentralWidget；当创建工程时使用QWidget时，也只能对它调用一次setLayout。

好在已经setLayout后，是可以对里面的widget动态修改的，qt官方的动态布局就是这样的。

又为了使用statusbar，所以还是继承QMainWindow。这样的话，就要通过setCentralWidget来实现动态布局，然而，再次setCentralWidget时，qt会自动销毁原来的CentralWidget，这样就和单例模式冲突了，因为后面再把界面切回来还要用到之前的实例的。

所以这里使用了套娃操作：

- mainWindowCentralWidget(类:QWidget 调用setCentralWidget将其设为主窗口的CentralWidget)
- - hBoxLayoutCentralWidget (类:QHBoxLayout)
  - - HomeWidget (类:HomeWidget继承QWidget 管理主窗口的控件布局，单例模式 )

这样的话，在切换界面时，只需要把QHBoxLayout里面的单例模式的widget换掉就行了。

同时需要注意的是，修改布局之后要把不用的widget的parent设置为null，否则它还会显示在界面上。

另外，为了共用槽函数，在切换widget的槽函数MainWindow::mainWindowSetLayout中，获取了发送信号的button的字符串用于判断当前点击的按键。

判断QHBoxLayout中当前的widget要用如下方法：

```
    QLayoutItem * item = hBoxLayoutCentralWidget->itemAt(0);
    QWidget *currentWidget = qobject_cast<QWidget*>(item->widget());
```

不能使用`hBoxLayoutCentralWidget->widget()`，这种只能获取到null。通过debug单步发现，HomeWidget并不是hBoxLayoutCentralWidget的children，而是mainWindowCentralWidget的children。

此外，绑定槽函数时，没有把当前界面对应的button绑定槽函数，这样点击它也就不起作用了，只有点击切换其他界面的button，才会生效。

## 相关信息

### 本次提交信息

本次提交hash值：7f4f3ae1a81c35e48c1c94c8b5c718aeb9a4fe97

### 功能

使用类管理不同的窗口/界面，使用单例模式。支持动态切换主窗口的布局。

### 操作方式

编译运行即可。

### 预期效果

能够正常显示按钮，点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

### 依赖

不依赖opencv。