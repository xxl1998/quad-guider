# 08 显示obj 彩色3D模型

## 编译安装assimp库

**Ubuntu可以尝试使用apt安装！！！在assimp的build.md里面有说。**

win11编译安装过程如下：

首先下载代码：（git下载了190MB）

```
https://github.com/assimp/assimp.git
```

然后切到release版本：（目前最新是5.2.5）

```
git checkout -b v5.2.5 v5.2.5
```

在assimp中新建两个文件夹build和install_dir。

然后打开cmake，使用图形界面配置：（Ubuntu的话可以用命令行）

首先配置源码路径（assimp文件夹），再配置编译文件存放路径（assimp里的build文件夹）

然后点击Configure，选择MinGW Makefiles，下面的选择Specify native compilers，点击next，分别指定C和C++的编译器，这里我的分别是`C:/Qt/Qt5.12.11/Tools/mingw730_32/bin/gcc.exe`和`C:/Qt/Qt5.12.11/Tools/mingw730_32/bin/g++.exe`，然后点击finish。

**这里选择的是qt的编译器，如果系统里有其他的C++编译器，并且存在环境变量里面，cmake就会报错说找不到编译器，这时需要去系统高级设置里面，添加qt的mingw的路径到环境变量里面去，例如：`C:/Qt/Qt5.12.11/Tools/mingw730_32/bin`，然后调整环境变量的顺序，使得刚刚添加的这个在别的上面，比如perl就自带一个mingw，把我们现在要使用的移到上面，就能消除perl的影响，而不需要去掉perl的边境变量，编译完assimp之后再把环境变量改回去就行了。改完环境变量之后，要把cmake的缓存清掉，然后重启一下cmake，再进行configure，就不会报错了。**

等待一段时间，直到下面输出Configuring done。cmake会自动运行一些测试并把可配置项显示出来，首先找到ASSIMP_BUILD_ZLIB并打开（如果没有这个库后面链接阶段会出错），然后翻到最下面，修改CMAKE_INSTALL_PREFIX为刚刚新建的install_dir文件夹（这些路径最好不要出现空格和中文字符）。

然后再次点击Configure，红色全部消失。点击generate，完成后关闭cmake。

在build目录下打开cmd或者powershell窗口，输入命令开始编译：

```
mingw32-make -j
```

然后安装：

```
mingw32-make install
```

我们需要的东西就全都安装到install_dir文件夹中了，可以在qt里面配置头文件和库的路径。

最终静态库是两个，一共700多KB，动态库15.6MB。

下面把头文件和库放到qt的目录中，这样就不用修改qt的pro文件了。

这里我的路径是`C:\Qt\Qt5.12.11\5.12.11\mingw73_32`，这里面是qt的头文件、静态库、动态库、可执行文件等，注意和前面的mingw的路径区分。

把`assimp\install_dir\bin`下面的libassimp-5.dll复制到`C:\Qt\Qt5.12.11\5.12.11\mingw73_32\bin`中。

把`assimp\install_dir\lib`下面的两个.a文件复制到`C:\Qt\Qt5.12.11\5.12.11\mingw73_32\lib`中。

把`assimp\install_dir\include`下面的assimp这个文件夹复制到`C:\Qt\Qt5.12.11\5.12.11\mingw73_32\include`中。

在pro文件中添加库：

```
LIBS += -L$$[QT_INSTALL_BINS]  -lassimp-5
```

如果qt没有加到环境变量中的话，还需要把库copy一份到可执行文件的目录下。

## 测试assimp库

创建一个新工程，引用assimp的头文件，然后new一个assimp里面的类就可以验证了。

也可以直接把modelloader.cpp和modelloader.h添加到工程中，能编译过就代表assimp的库配置OK了。

## 特别感谢

除了assimp外，还使用了两位开发者的代码，原作者**应该**是这篇文章的作者[闪电的蓝熊猫](https://www.jianshu.com/u/791d3187121d)：

https://www.jianshu.com/p/478f63781204

但是，这位博主**似乎**没有把代码放到github/gitee，而只是分享了网盘。

所以，就有了另一位博主[章小京](https://blog.csdn.net/pengrui18)发的这个视频：

https://www.bilibili.com/video/BV1Jb4y197KS

这位博主**应该**是把前面那位博主的代码加以整理（见https://gitee.com/pengrui2009/open-gl-study/blob/master/README.md），并且单独构建了一个qt工程：

https://gitee.com/pengrui2009/qopen-gl3-d.git

这位博主在CSDN和仓库里面也标注了原作者，感谢这位博主的工作，使得这些代码保留了下来（原来的网盘链接已经失效了），而且这个qt工程也极大的节省了我的工作量。所以除了感谢assimp之外，在这里也对这两位博主表示感谢。

## 移植过程

### 显示模型

首先参考https://gitee.com/pengrui2009/qopen-gl3-d.git这个工程，以下简称demo。

在pro文件中修改库的配置后，就能够把demo编译通过了，接着按照作者的说明把相关文件复制到可执行文件目录，再把assimp的dll放到可执行文件目录，就能跑起来了。然而，模型无法加载，一通操作之后，发现文件路径在传的过程中传没了，改了后发现相对路径读不到，暂且先改成绝对路径跑起来了，能够正常显示彩色的卡车3D模型，不过没有视频里面自动旋转的效果，而是静态的。

写过OpenGL的应该知道，可以对物体进行平移旋转，也可以配置观察的摄像头的位置和朝向，这样就可以做出想要的效果了。预期效果为：显示在状态界面的中间；显示彩色的四旋翼模型；显示由三个矩形组成的“墙角”，方便查看四旋翼模型相对于坐标系的姿态变化；摄像机观察的位置可以变化，方便调整视角；通过摄像机距离模型的位置实现视角的缩放；模型的旋转指示飞控的实时姿态；在界面中使用鼠标可以进行视角调整和缩放。

根据小步快走的开发原则，本次先实现模型显示（嵌入到当前的程序中的第二个界面status page），剩下的功能下次实现。

在分析demo代码后，首先添加了自动旋转的功能，发现这个程序在拉伸窗口时模型并不会变形，这点非常好。（后面用到的qt的example里的立方体是会变形的）

**飞机模型：**

这里模型我使用的是一个非常简单的固定翼飞机模型，模型来自于：

https://free3d.com/3d-model/simple-jet-model-25871.html

我使用blender在上面简单添加了颜色，并导出了obj和mtl文件来使用。

### 模型的旋转

看到代码里面的lookat、viewport、translate等函数，想起之前写OpenGL程序的时候，最开始使用自带的小茶壶模型，以及后面使用stl模型的经历，很顺利就实现了模型的旋转、位移、视角变化等功能。只是这里是通过对矩阵进行操作实现的。

### 显示网格线

在移植代码之前忍不住又写了一个显示网格线的功能，在原点和坐标轴正方向形成的“墙角”上的三个平面上绘制网格线，可以更清晰地呈现原点和坐标系，从而更清楚模型的位置、姿态、以及当前观察的视角。这个实现过程也比较曲折，首先简单画了个三角形，把坐标点放大后看到它是跟着飞机模型转动的。

为了让三角形不动，只有模型转动，有两种方法：将三角形（网格线）按反方向转动；单独渲染三角形（网格线）。为了熟悉代码方便移植，选择单独渲染。把着色器、VBO、VAO等都复制了一遍出来，又搜了N多教程，终于能够显示不动的直线了。

接着就是使用代码动态生成要画的直线的顶点的坐标，这就涉及到了动态内存分配的问题，在写代码的时候错把方括号写成了圆括号，导致程序直接崩了。。正确写法：`pointer = new float[36]();`方括号里面是申请的大小，圆括号是初始化的。这个问题解决之后，网格线就能正常显示了。

### 使用QWidget

#### 使用Textures Example

demo里面是继承的QWindow，无法嵌入到现有的代码框架里面，只有把它变成继承QWidget的，才能嵌入到当前窗口中去渲染模型。本来打算直接改成继承QWidget，但是，里面context、surface等等东西很难处理，好不容易编译成功了，跑起来各种crash、ASSERT过不去等等一堆问题。。

（也考虑过使用tinyobjloader，但是这个项目没有找到基于qt的demo，就放弃了）

这时候我意识到要想大力出奇迹是不行了，只能对这个代码有足够的了解，才能继续往下走。所以改变策略，一方面在精简代码段过程中熟悉代码，另一方面找到在qt的窗口中嵌入OpenGL渲染的程序为基础去修改。在qt的example里面果然找到了合适的代码，工程是“Textures Example”，里面是使用了一个QWidget作为主窗口，在里面嵌入了六个继承QOpenGLWidget的控件，能够渲染六个彩色的立方体。

将qt的example一顿魔改，成功把它变成了以QMainWindow为主窗口，在一个QWidget里面渲染OpenGL的形式，与目标场景一致。

#### 显示网格线

将代码一点点移植过来，modelloader并没有问题，整个拿过来就行了，但是scene.cpp里面需要一点点手动移植，好在它们之间都是OpenGL代码，存在共同之处，把初始化、更新、成员变量、函数一个个移植过来，编译过了之后，一运行就崩，只能先把渲染模型的注释掉。

然后是解决着色器代码编译出错的问题，为了减少文件数量，我把着色器的代码以字符串形式写进了cpp文件中，对比发现是因为缺少了第一行的`#version 330 core`，再移植的时候把它当成注释了。。。

然后把渲染立方体的注释掉，发现一片黑，将背景色设置为白色后，发现有黑色轮廓，形状与之前画的网格线很相似，想到之前有个没生效的设置线宽200的，注释掉之后网格线就能够正常显示了。再一通debug，把问题限制在了渲染模型的glDrawElements这一行代码。

#### 加载模型问题

由于网格线能够显示，而且视角正常，所以基本可以认为OpenGL的环境没有问题，着色器也没有问题，同时看到加载的几个buffer的size都是正确的，说明assimp读取的数据应该也没问题，那么问题就集中到了VAO、VBO上面去了。这时尝试使用`glDrawArrays( GL_TRIANGLES, 0,144);`直接使用buffer中的数据去画三角形，但是没有明显现象。

为了简化问题，特意去blender里面导出了一个红色立方体的简单模型。这样的话就只有36个点，再使用glDrawArrays就出现了明显的现象，画出了几个三角形，而且颜色是红色的。这就表明顶点数据在buffer里面是有的，只是调用glDrawElements的时候出了问题。

仔细查看了glDrawElements的传参，查找了相关资料找到了glDrawElements的两种用法，一个是使用一个数组存储顶点的索引（数组里面的元素分别是要使用的顶点的序号），另一个用法是glDrawElements配合EBO使用，在初始化时将索引放到EBO里面，之后渲染的时候使用EBO里数据去索引顶点。（可以简单理解为：VBO存顶点，EBO存索引）

https://blog.csdn.net/birdflyto206/article/details/51191880（glDrawElements+数组）

https://www.bilibili.com/read/cv10477263（glDrawElements+EBO）

那么显然我们要用的是EBO，但是，在代码里面找了半天，也没找到EBO。。。最后灵光一闪，好像发现了一个没什么存在感的变量，再一对比发现，它使用QOpenGLBuffer::IndexBuffer构造的，再看下这个代码：

https://github.com/Italink/QtOpenGL-Essential-Training/blob/main/Day02/widget.cpp

果然，m_indexBuffer在构造的时候要指定为QOpenGLBuffer::IndexBuffer，它就是EBO。。问题引入是因为C++还太菜，移植的时候漏了东西。后面深究了EBO的初始化才找到问题。

将EBO的构造加上之后，顺利地显示出了红色立方体。

#### 颜色问题

在立方体模型能够正常显示之后，换回了飞机模型，却发现只有螺旋桨有颜色，而且只有红色，第一反应是着色器可能在移植过程中出错了，又把独立窗口的代码跑起来对比，发现这个显示效果相同，这才确定是模型的问题。由于是通过改文件名换的模型，而obj模型文件里面包含了mtl的文件名（文件是ASCII的，打开就能看到），导致加载错了mtl。将文件名都改回去，就能够正常显示飞机的颜色了。（所以要改模型的话还是去blender重新导出吧。。）

#### 总结

要想让模型能够正常显示，需要保证：VBO里数据正常（modelloader正常）、VAO正常（调用bind时机和顺序正确）、着色器正常（着色器代码、link等）、buffer索引正常（EBO的操作、着色器的setAttributeBuffer等）、其他代码功能正常。OpenGL3.3的学习曲线还是太陡峭了，入门阶段比较痛苦，而且数据都在显卡那边，调试手段比较有限，一不小心就crash或者啥都显示不了。当然好处就是更加灵活、能够支持更高性能、更大规模的渲染。

另外，原本的demo是继承的QWindow，而且兼容了OpenGL2.1/OpenGL ES。由于继承的QWindow，不能使用OpenGL自带的initializeGL、paintGL等函数，还需要配置一堆的context、surface等东西，刷新也是通过一个定时器手动去刷新的，没有OpenGL里面一个update那么优雅。由于这个显示3D模型的功能是为了在上位机上面方便查看飞控姿态的，所以只在PC上面运行，就不添加OpenGL ES的支持了，而且OpenGL3.3是2010年的版本，现在的电脑应该不会有更低的版本了。

### 添加到程序中

移植的时候只使用了两个文件modelloader.cpp和modelviewwidget.cpp，将无用的代码和注释删掉，顺便把鼠标移动事件写上了，刚好可以移动视角。

把源文件和头文件添加到程序中。由于前面移植的时候已经考虑到了当前代码的UI结构，所以添加一个widget就能很容易地实现显示模型了。但是也发现了新的问题，一个是模型的控件太小，这个通过设置最小size解决。

另一个问题是切换界面后会调用initializeGL，导致无法显示，甚至程序崩溃。这个解决思路倒也明确，那就是重新加载一遍这个控件，这里用到的方法和主界面中叠加显示的类似，就是新添加一个widget，让ModelViewWidget在这个widget上面显示，则其它的布局和控件都不需要修改。

此外还添加了一个label用于显示当前camera的位置，方便后面添加设置的时候可以把camera位置等加入配置文件的支持。

下一步工作：完善ModelViewWidget的析构函数，防止内存泄露；支持鼠标滚轮缩放；支持camera位置的更新；把编译好的库上传git；添加assimp的license，readme中添加assimp的说明；使用条件编译控制是否引入assimp。

## 相关信息

### 本次提交信息

本次提交hash值：92208cacdc6b0225606a9eb0d971da198bb1535e

### 功能

完善状态界面的内容，添加显示飞机3D模型的控件，支持obj格式的彩色模型。

### 操作方式

编译运行即可。

### 预期效果

主界面：能够正常显示按钮，能够正常显示左右两侧的label，能够正常显示叠加在最底层的图片，能够正常显示背景透明的曲线。按钮外观：蓝色边框，黑色字体，鼠标悬停时改变颜色，代表当前窗口的按钮是disabled状态，显示明显的蓝色。

点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

状态界面：能够显示彩色飞机3D模型，在模型上面拖动鼠标可以改变视角，模型下面的label中显示观察模型的camera的初始位置。多次切换到状态界面，3D模型都能正常显示。

### 依赖

qt需要charts，对于Ubuntu来说可能需要使用源码编译安装。

需要assimp库，Windows需要自行编译安装。

需要OpenGL3.3。