# 03 使用代码布局（不使用UI文件）+状态栏

## 技术要点

### 去掉UI相关的内容

之前的工程中有Qt Creator自动生成的UI文件，需要先把相关内容删掉。

### 手动布局

在mainwindow.h文件中添加public的成员变量：

```
public:
    QLabel *labelPicture;
    QStatusBar *windowStatusBar;
    QLabel *labelStatusHostPlatform;
    QLabel *labelStatusArmed;
    QLabel *labelStatusFlyMode;
    QLabel *labelStatusUDPSpeed;
    QLabel *labelStatusSerialSpeed;
```

在mainwindow.cpp中创建相关对象：

```
labelPicture = new QLabel();
```

然后可以设置文本内容，或者使用图片填充：

```cpp
labelStatusUDPSpeed->setText("UDP:0B/s");
```

### 状态栏

mainwindow继承了QMainWindow这个类，里面已经包含了状态栏，只需要获取到指针，然后在上面添加内容就行了。

在mainwindow.h文件中添加成员变量和包含头文件：

```
#include <QStatusBar>

QStatusBar *windowStatusBar;
```

在mainwindow.cpp中获取QStatusBar的指针，然后往状态栏上面添加label：

```
    windowStatusBar = statusBar();
    labelStatusArmed = new QLabel();
    labelStatusArmed->setText("Disarmed");
    windowStatusBar->addWidget(labelStatusArmed);
```

## 相关信息

### 本次提交信息

本次提交hash值：3540a20f094949c35031e49b51968001fad1c494

### 功能

使用代码直接布局，方便git的版本管理；添加状态栏支持。

### 操作方式

编译运行即可。

### 预期效果

窗口大小1280*720，能够显示图片，能够显示状态栏文字。

### 依赖

依赖自行编译的opencv库，参考xxx文章。（TODO）