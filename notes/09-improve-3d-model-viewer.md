# 09 完善3D模型视图

## 技术要点

上次是在Windows下提交的obj模型，导致里面的LF都变成了CRLF，特意从gitee上面下载回来试了一下，还是可以正常加载的。（这里表述有点问题，git的autocrlf功能应该是使得Windows下面当前可见文件变成CRLF，而保存到git仓库里的文件在checkout的时候换行符随平台自动变化，这个在到Ubuntu下面的时候再验证）

### 支持条件编译

需要在pro文件中进行配置，从而影响到工程文件的添加以及编译时宏定义控制的条件编译。

pro文件配置如下：

```
DEFINES += USE_ASSIMP_FOR_3D_MODEL_VIEWER

if(contains(DEFINES,USE_ASSIMP_FOR_3D_MODEL_VIEWER)){
SOURCES += \
    modelloader.cpp \
    modelviewwidget.cpp

HEADERS += \
    modelloader.h \
    modelviewwidget.h
}
```

在C++代码中，可以使用`USE_ASSIMP_FOR_3D_MODEL_VIEWER`这个宏定义做条件编译。

### 支持鼠标滚轮

首先完善了一下鼠标拖动的逻辑，把鼠标右键拖动也添加上了，鼠标左键和右键可以覆盖到camera的x、y、z三个坐标的移动，这样视角就能任意调整了。

支持鼠标滚轮缩放也比较简单，重写一下`wheelEvent(QWheelEvent *event)`这个函数，在里面判断`event->delta()`的正负号，将camera坐标整体缩放就行了。根据实际测试来看，鼠标滚轮转动15度产生一次回调，delta()的值是120或-120，实际上只用它的正负号就行了，数值并没有太大作用。

### 支持camera位置的更新

好在目前ModelViewWidget这个类里面已经有了clicked信号，添加一个槽函数去修改label的字符串就可以了。需要注意的是，在remove的时候要把连接断开。此外，在鼠标滚轮事件中也发送了clicked信号，使得鼠标缩放时也可以更新label上的字符串。

### 上传库文件

考虑到大多数情况还是在Windows下开发，为了能够降低门槛，还是把assimp编译好的库放到仓库里面，同时也修改了pro文件中的配置，使用仓库中的头文件和库文件。要让程序能够运行的话，还需要把dll文件copy一份放到executable目录下（通过qt creator启动的话不需要）。

## 相关信息

### 本次提交信息

本次提交hash值：6fc4c0409cf264251dc73077d14ad50f35c6696f

### 功能

完善3D模型控件的功能，支持鼠标右键拖动、鼠标滚轮缩放，支持camera位置的更新。

### 操作方式

编译运行即可。

### 预期效果

主界面：能够正常显示按钮，能够正常显示左右两侧的label，能够正常显示叠加在最底层的图片，能够正常显示背景透明的曲线。按钮外观：蓝色边框，黑色字体，鼠标悬停时改变颜色，代表当前窗口的按钮是disabled状态，显示明显的蓝色。

点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

状态界面：能够显示彩色飞机3D模型，在模型上面拖动鼠标可以改变视角，鼠标右键拖动以另一种方式改变视角，鼠标滚轮可以整体缩放视角，鼠标释放后模型下面的label中更新观察模型的camera的位置。多次切换到状态界面，3D模型都能正常显示。

### 依赖

qt需要charts，对于Ubuntu来说可能需要使用源码编译安装。

需要assimp库，Windows需要自行编译安装。

需要OpenGL3.3。