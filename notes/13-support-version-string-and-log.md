# 13 支持版本信息&log功能

有这个想法很久了，在这里实现一下。最初是看到u-boot、kernel启动时都会打印编译主机、编译时间、版本信息等，在写这个程序的时候就在想要加上类似的功能。

至于log功能，则是目前打印都是通过qt的API，会打印大量消息，不方便保存、查找，所以需要实现一个打印log到文本文件的功能，打印格式类似Android的logcat。最终可以实现log内容分级，科学管理log打印。

## 技术要点

### 支持版本信息

#### 编译前调用脚本

bing中文搜索，搜到的都是这样的：

```
QMAKE_PRE_LINK += $$quote($$PWD/../scripts/update_version.bat)
QMAKE_POST_LINK +=  $$quote($$PWD/../scripts/update_version.bat)
```

然而，这是链接前后调用的，此时源码已经编译完成了，头文件不会再生效了。。。

使用某英文搜索引擎在StackOverFlow上面找到了答案：

```
version_update.commands = $$PWD/../scripts/update_version.bat
version_update.depends = FORCE
QMAKE_EXTRA_TARGETS += version_update
PRE_TARGETDEPS += version_update
```

这种写法可以在编译前执行脚本，达到需求。

#### 获取git仓库版本的字符串

参考kernel的脚本：

https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/scripts/setlocalversion?h=v6.1-rc2

关键命令：

测试是否存在git仓库：

```
$ git rev-parse --show-cdup
../
```

获取最新提交的HASH值（短）：

```
$ git rev-parse --verify --short HEAD
c7b06e5
```

测试是否存在未提交的代码：

```
$ git --no-optional-locks status -uno --porcelain
 M notes/12-support-mavlink-inspector-in-scroll-area.md
 M source/settingswidget.h
```

简单起见，版本号就在代码里写死了，这样上面几条命令就足够用了。最终字符串通过拼接获得。

#### bat脚本

目前主要在Windows下开发，所以暂时只写bat脚本，等功能完善后添加打包全部资源（可执行程序、媒体资源、dll、配置文件等）的脚本，再到Ubuntu下测试并补上相关脚本。

吐槽一下bat脚本的语法实在难搞。为了文本文件（头文件）中的字符串查找、获取行号、行号加1、替换整行这几个功能，折腾了好久。

#### 区分debug与release版本

qt自带的这个宏可以用于区分debug与release版本：

```C++
#ifdef QT_NO_DEBUG
    versionString += "-release";
#else
    versionString += "-debug";
#endif
```

之后就是把字符串拼接起来，放进label里显示。

配置label文字可以被鼠标选中复制：

```
labelProgramVersion->setTextInteractionFlags(Qt::TextSelectableByMouse);
```

最终的版本字符串是这样的：

```
V0.1-c7b06e5-dirty-windows-debug
```

#### 忽略version.h的变化

本来以为写进.gitignore就行了，结果它只能忽略没有提交的文件，对于已经提交的文件，可以使用这条命令忽略：

```
git update-index --assume-unchanged ../source/version.h
```

但是不想用这个命令，因为这个修改了git仓库配置（用户可能不知道），搜了一圈没有发现合适的办法，暂时使用一个不太优雅的办法：

新建一个`source/version_.h`并提交到git仓库，bat脚本中从`source/version_.h`读取并将修改后的内容写入`source/version.h`，`.gitignore`中忽略`source/version.h`，代码中引用`source/version.h`。

### log功能

简单搜索了一下，暂时没有找到合适的开源代码。目前已有的方案有：

1. 使用qInstallMessageHandler重写qt的log输出，我还想保留qt原来的log打印，所以不用这个；
2. log4qt，log4j在qt上面的实现与扩展，这个太过于复杂了，我想要的是只有一个cpp一个头文件的轻量级的；
3. 网友自己写的，看了几个都是直接写入文件的，没有考虑线程安全以及写入log占据了程序运行时间。

所以还是决定自己写一个，也方便之后使用。

规划功能如下：

- log类运行在子线程中，保证不占用主线程UI刷新的时间；
- 支持多种log等级（类似logcat）；
- 支持多线程访问（线程安全）；
- 支持指定log文件名，支持随时写入新log文件；
- 默认以当前时间决定文件名；
- 支持中文；
- 支持配置log文件路径（默认在exe/log/date-time.log）；
- 每条log自带时间戳。

具体的实现，用前面用过的技术：单例模式、多线程、读写锁、链表、获取时间。

新的技术点也就是文本文件的写入了。这次子线程使用的是继承QThread重写run函数的方法，前面几个功能的组合，没有出现什么问题。但是文本文件的创建比较坑，使用QFile需要先创建文件再打开，创建的方法就是打开再关闭（C++提供的ofstream就可以自动创建）。而且文件夹不存在时需要自己创建，后面文件夹创建成功了，文件还是创建失败，这才发现是文件名中含有冒号导致失败。。。。

调试的时候出现了低概率的crash问题（大约十几次出现一次），检查发现是因为程序初始化时QFile指针不为null，程序启动时打开log文件中运行了一次关闭文件的函数，导致使用了异常的指针，需要在构造函数中将用到的变量都初始化。

在log模块完成后，把程序中的log输出改了一下。以后的调试思路是新代码同时打印到文件和qDebug，测试没问题后改成：所有log都输出到文件，错误log同时打印到qDebug。

## 本次提交信息

本次提交hash值：（TODO）

### 新增功能

设置页面支持显示版本字符串，支持打印log到文件，支持log分级。

### 操作方式

程序启动后，点击顶部按钮切换到设置界面。

### 预期效果

主界面：能够正常显示按钮，能够正常显示左右两侧的label，能够正常显示叠加在最底层的图片，能够正常显示背景透明的曲线。按钮外观：蓝色边框，黑色字体，鼠标悬停时改变颜色，代表当前窗口的按钮是disabled状态，显示明显的蓝色。

点击其他界面的按钮能够切换界面，点击本界面的按钮无反应，界面可以反复切换。

状态界面：能够显示彩色飞机3D模型，在模型上面拖动鼠标可以改变视角，鼠标右键拖动以另一种方式改变视角，鼠标滚轮可以整体缩放视角，鼠标释放后模型下面的label中更新观察模型的camera的位置。多次切换到状态界面，3D模型都能正常显示。能够显示MavLink消息列表（可以正常更新各消息的刷新率），点击某一消息能够显示MavLink消息的详细内容。

网络界面：点击set按钮，能够在文本框中显示出UDP收到的原始数据，主界面的label显示的姿态角能够正常更新。

设置界面：能够读取到当前使用的配置文件，可以在编辑区修改，点击按钮可以设置为新的配置文件，当文件不存在时会弹窗警告，设置成功时无提示或报错。编辑区只支持输入数字字母下划线。**能正常显示版本信息字符串。**

分离窗口：双击顶部按钮可以分离出新窗口，所有窗口地位等同，都可以任意切换界面，关闭某个窗口不影响其他窗口。

### 依赖

#### 编译依赖：

qt需要charts，对于Ubuntu来说可能需要使用源码编译安装。

可配置是否使用assimp库，使用assimp库时需要OpenGL3.3，不使用assimp时无法显示彩色3D飞机模型。

#### 运行依赖：

使用MavLink相关功能时，需要有PX4的MavLink数据流，可以使用此项目中包含的测试程序mavlink-sender模拟飞控发送的UDP数据包，也可以是源码编译的PX4 sitl或者连接了UDP数传的运行PX4的飞控。

程序运行时用到的资源文件已包含在项目仓库中。